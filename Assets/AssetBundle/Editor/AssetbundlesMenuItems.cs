﻿using UnityEngine;
using UnityEditor;

namespace AssetBundles
{
    public class AssetBundlesMenuItems : EditorWindow
    {
        const string kSimulationMode = "Assets/AssetBundles/Simulation Mode";


        [MenuItem("Tools/Build AssetBundles %#q")]
        static public void BuildAssetBundles()
        {
            txtStyle = new GUIStyle();
            txtStyle.alignment = TextAnchor.UpperCenter;
            txtStyle.fontStyle = FontStyle.Bold;
            txtStyle.normal.textColor = Color.white;

            EditorWindow window = GetWindow(typeof(AssetBundlesMenuItems), true, "Delete assetbundle");
            window.position = new Rect(Screen.width / 2, Screen.height / 2, 250, 150);
            //BuildScript.BuildAssetBundles();
        }

        [MenuItem("Assets/AssetBundles/Delete All assets in assetbundle")]
        static public void DeleteAssetbundle()
        {
            //txtStyle = new GUIStyle();
            //txtStyle.alignment = TextAnchor.UpperCenter;
            //txtStyle.fontStyle = FontStyle.Bold;
            //txtStyle.normal.textColor = Color.white;

            //EditorWindow window = GetWindow(typeof(AssetBundlesMenuItems), true, "Delete assetbundle");
            //window.position = new Rect(Screen.width /2, Screen.height / 2, 250, 150);
            
        }
       static  GUIStyle txtStyle = new GUIStyle();


        void OnGUI()
        {            
            GUILayout.Label("Build Asset platform " + EditorUserBuildSettings.activeBuildTarget, txtStyle, GUILayout.Width(250));
            GUILayout.BeginHorizontal(txtStyle);
            if (GUILayout.Button("Yes", GUILayout.Width (60)))
            {
                BuildScript.BuildAssetBundles();
                //BuildScript.DeleteAssetbundle();
                Close();
            }
            else if (GUILayout.Button("No", GUILayout.Width(60)))
            {
                Close();
            }
            GUILayout.EndHorizontal();

        }
    }
}