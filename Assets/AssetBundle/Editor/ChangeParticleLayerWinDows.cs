﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Linq;
using Boo.Lang;

public class ChangeParticleLayerWinDows : EditorWindow {
    public Transform[] particles;
    static string[] _choices;
    static int _choiceIndex = 0;

    private List<int> lstLayer;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Tools/Change Particle Layer")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        ChangeParticleLayerWinDows window = (ChangeParticleLayerWinDows)EditorWindow.GetWindow(typeof(ChangeParticleLayerWinDows));
        if (_choices == null)
        {
            _choices = SortingLayer.layers.Select(x => x.name).ToArray();
        }
        window.Show();
    }

    private SerializedObject so;
    private SerializedProperty stringsProperty;
    private bool isExpa;
    void OnEnable()
    {
        ScriptableObject target = this;
        so = new SerializedObject(target);
        stringsProperty = so.FindProperty("particles");
        stringsProperty.isExpanded = false;
        lstLayer = new List<int>();
    }
    void OnGUI()
    {
        GUILayout.Label("Select layer", EditorStyles.boldLabel);
        if (_choices == null)
        {
            _choices = SortingLayer.layers.Select(x => x.name).ToArray();
        }
        _choiceIndex = EditorGUILayout.Popup(_choiceIndex, _choices);

        GUILayout.Space(20);
        GUILayout.Label("Particle to change", EditorStyles.boldLabel);


        isExpa = stringsProperty.isExpanded;
        EditorGUILayout.PropertyField(stringsProperty, isExpa); // True means show children
        

        so.ApplyModifiedProperties(); // Remember to apply modified properties
        GUILayout.Space(20);
        if (Event.current.type == EventType.DragPerform) { return; }
        for (int i = 0; i < stringsProperty.arraySize; i++)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.ObjectField(stringsProperty.GetArrayElementAtIndex(i));
            if (lstLayer.Count <= i)
            {
                for (int j = 0; j <= i - lstLayer.Count; j++)
                {
                    lstLayer.Add(0);
                }
                
            }

            lstLayer[i] = EditorGUILayout.IntField(lstLayer[i]);
            EditorGUILayout.EndHorizontal();
        }
        


        GUILayout.Space(20);
        if (GUILayout.Button("OK"))
        {
            for (int i = 0; i < particles.Length; i++)
            {
                int layer =  lstLayer[i];
                change(particles[i], _choices[_choiceIndex], layer);
            }
        } 
    }

    
    void change(Transform c, string layer, int sortLayer)
    {

        Debug.Log("change " + c.name);
        Renderer ren = c.GetComponent<Renderer>();
        if (ren != null)
        {
            ren.sortingLayerName = layer;
            ren.sortingOrder = sortLayer;
        }
        
        for (int i = 0; i < c.childCount; i++)
        {
            change(c.GetChild(i), layer, sortLayer);
        }

    }
}
