﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestScript : MonoBehaviour
{
    public Button btnTest = null;

    private void Awake()
    {
        btnTest.onClick.AddListener(onBtnTestClick);
    }

    private void onBtnTestClick()
    {
        actionCallBack(5);
    }

    private Action<int> actionCallBack;

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            initButton(showSpace);
        }
        if (Input.GetKeyUp(KeyCode.K))
        {
            initButton(showK);
        }
    }

    public void initButton(Action<int> callBack)
    {
        actionCallBack = callBack;
    }



    private void showSpace(int number)
    {
        Debug.LogError("show space");
    }

    private void showK(int number)
    {
        Debug.LogError("show K");
    }






}
