﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using DG.Tweening;

public class ItemEventThumbView : MonoBehaviour {

	[SerializeField]
	LayoutElement element;
	[SerializeField]
	RectTransform rect;
	[SerializeField]
	Image img;
	[SerializeField]
	Button btn;

	onCallBackInt click;

	bool _isSelected;

	public int Index {
		get;
		set;
	}
	public string UrlSpr {
		get;
		set;
	}
	public static Sprite Spr {
		get;
		set;
	}

	const float TIME_MOTION = .3f;
	const float MIN_HEIGHT = 160;
	const float MAX_HEIGHT = 210;

	onCallBack complete;

	public void Init(int _index, string _urlSpr, onCallBackInt _click){
//		Debug.Log ("INIT - " + _index);
		Index = _index;
		UrlSpr = _urlSpr;

		btn.onClick.AddListener (BtnOnClick);

		click = _click;

		_isSelected = false;

//		rect.anchoredPosition = new Vector2(_index % 2 == 0 ? 70f : -70f, 0);
		rect.anchoredPosition = new Vector2(0, 0);
	}

	public void ShowSprite(Sprite _spr)
	{
		if (Spr == null)
			StartCoroutine (GameHelper.Thread (UrlSpr, OnShow));
		else {
			Spr = _spr;
			ShowSprite (Spr);
		}
	}
	void OnShow(Sprite _spr){
		img.sprite = _spr;
	}

	public bool IsSelected {
		set {
			_isSelected = value; 
			SetSelected (value);
		}
		get{ return _isSelected; }
	}

	void SetSelected(bool _enable)
	{
		if (_enable) {
			element.DOPreferredSize (
				new Vector2 (10, MAX_HEIGHT), TIME_MOTION).SetEase (Ease.OutBack);

			rect.DOSizeDelta (new Vector2 (247, 219), TIME_MOTION).SetEase (Ease.OutBack);
		} else {
			element.DOPreferredSize (
				new Vector2 (10, MIN_HEIGHT), TIME_MOTION).SetEase (Ease.InBack);

			rect.DOSizeDelta (new Vector2 (177, 157), TIME_MOTION).SetEase (Ease.InBack);
		}
	}

	void BtnOnClick()
	{
		click (Index);
	}


}
