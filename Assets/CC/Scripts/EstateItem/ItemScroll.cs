﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemScroll : MonoBehaviour {
    [SerializeField]
    GameObject Content, ItemHome, ItemShop, ItemCar;
    [SerializeField]
    MadControll Mad;
    Dictionary<string, Sprite> ListSpriteItemHome;
    Dictionary<string, Sprite> ListSpriteItemShop;
    Dictionary<string, Sprite> ListSpriteItemCar;
    Dictionary<string, GameObject> DicHomeItem;
    Dictionary<string, GameObject> DicShopItem;
    Dictionary<string, GameObject> DicCarItem;

    [SerializeField]
    EstateControll Estate;

    public void InitContent(float left, float right)
    {
        //Content.GetComponent<RectTransform>().
    }


    public void InitItemHomeCroll()
    {
        
        
        
        if (DicHomeItem != null)
        {
            foreach (var item in DicHomeItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicShopItem != null)
        {
            foreach (var item in DicShopItem)
            {
                Destroy(item.Value);
            }
        }

        if (DicCarItem != null)
        {
            foreach (var item in DicCarItem)
            {
                Destroy(item.Value);
            }
        }


        
        DicHomeItem = new Dictionary<string, GameObject>();


        // Hien Vat Pham so Huu
        foreach (My_Item_SoHuu item in DataHelper.Array_DuLieuSoHuu)
        {

            My_Item_SoHuu it = item;
            if (it.type == "house")
            {

                InitItemHome(it.idTong, it.Level.ToString(), it.idRieng, false);
            }
        }

        // Hien Vat Pham chua so Huu

        foreach (var item in DataHelper.DuLieuNha)
        {
            if (item.Key != "5dc03420e52b8e2d238b6045")
            {
                InitItemHome(item.Key, "0");
            }

        }

    }


    public void InitItemShopInScoll()
    {
        ListSpriteItemShop = DataHelper.dictSprite_Shop;
        if (DicHomeItem != null)
        {
            foreach (var item in DicHomeItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicShopItem != null)
        {
            foreach (var item in DicShopItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicCarItem != null)
        {
            foreach (var item in DicCarItem)
            {
                Destroy(item.Value);
            }
        }

        DicShopItem = new Dictionary<string, GameObject>();


        // Hien Vat Pham so Huu
        foreach (My_Item_SoHuu item in DataHelper.Array_DuLieuSoHuu)
        {

            My_Item_SoHuu it = item;
            if (it.type == "shop")
            {

                InitItemShop(it.idTong, it.Level.ToString(), it.idRieng, false);
            }
        }


        // Hien Vat Pham chua so Huu

        foreach (var item in DataHelper.DuLieuShop)
        {
           
            InitItemShop(item.Key, "0");
        }





    }



    public void InitItemCarInScoll()
    {
        ListSpriteItemCar = DataHelper.dictSprite_Car;
        if (DicHomeItem != null)
        {
            foreach (var item in DicHomeItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicShopItem != null)
        {
            foreach (var item in DicShopItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicCarItem != null)
        {
            foreach (var item in DicCarItem)
            {
                Destroy(item.Value);
            }
        }


        DicCarItem = new Dictionary<string, GameObject>();

     

        // Hien Vat Pham so Huu
        foreach (My_Item_SoHuu item in DataHelper.Array_DuLieuSoHuu)
        {

            My_Item_SoHuu it = item;
            if (it.type == "car")
            {
                InitItemCar(it.idTong, it.Level.ToString(), it.idRieng, false);
            }
        }
        // Hien Vat Pham chua so Huu

        foreach (var item in DataHelper.DuLieuXe)
        {
            //if (item.Key != "5dc03420e52b8e2d238b6045")
            //{
            //    InitItemHome(item.Key, "0");
            //}
            InitItemCar(item.Key, "0");
        }




    }










    public void InitItemHome(string IdTong, string level, string Id = "", bool clock = true)
    {
        GameObject Obj = Instantiate(ItemHome) as GameObject;
        // Obj.transform.SetParent(Content.transform);
        Obj.GetComponent<RectTransform>().SetParent(Content.GetComponent<RectTransform>().transform);
        Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
 
        ItemShopHome MyItem = Obj.GetComponent<ItemShopHome>();


        My_Item it = DataHelper.DuLieuNha[IdTong];



     //   Sprite spr = DataHelper.dictSprite_House[Id];


        MyItem.Init(IdTong, clock, level, false, 0, Id);

        if (Id == "")
        {
            DicHomeItem.Add(IdTong, Obj);
        }
        else
        {
            DicHomeItem.Add(Id, Obj);
        }
       
        
    }

    public void InitItemShop(string IdTong, string level, string Id = "", bool clock = true )
    {
        GameObject Obj = Instantiate(ItemShop) as GameObject;

        Obj.GetComponent<RectTransform>().SetParent(Content.GetComponent<RectTransform>().transform);
        Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        
        ItemShop MyItem = Obj.GetComponent<ItemShop>();

       
        My_Item it = DataHelper.DuLieuShop[IdTong];
        

       // Sprite hinh = ListSpriteItemShop[Id];
        string ten = it.ten;
        long gem = it.gem;
        long chip = it.chip;
        int diem = it.diemtaisan;
        string thongtin = it.thongtin;


        MyItem.Init(IdTong, clock, level, false, 0, Id);

        if (Id == "")
        {
            DicShopItem.Add(IdTong, Obj);
        }
        else
        {
            DicShopItem.Add(Id, Obj);
        }

        
       
    }

    public void InitItemCar(string IdTong, string level, string Id = "", bool clock = true)
    {     
        GameObject Obj = Instantiate(ItemCar) as GameObject;
        Obj.GetComponent<RectTransform>().SetParent(Content.GetComponent<RectTransform>().transform);
        Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        
        ItemShop MyItem = Obj.GetComponent<ItemShop>();

        
        My_Item it = DataHelper.DuLieuXe[IdTong];

        MyItem.InitCar(IdTong, clock, level, false, 0, Id);

        if (Id == "")
        {
            DicCarItem.Add(IdTong, Obj);
        }
        else
        {
            DicCarItem.Add(Id, Obj);
        }



        
    }

}
public class SortDuLieuSoHuu : IComparer
{
    public int Compare(object x, object y)
    {
        My_Item_SoHuu p1 = x as My_Item_SoHuu;
        My_Item_SoHuu p2 = y as My_Item_SoHuu;

        int gem_1 = (int)p1.gem;
        int gem_2 = (int)p2.gem;

        if (gem_1 < gem_2)
        {
            return 1;
        }
        else if (gem_1 == gem_2)
        {
            return 0;
        }
        else if (gem_1 > gem_2)
        {
            return -1;
        }

        return 0;
    }
}