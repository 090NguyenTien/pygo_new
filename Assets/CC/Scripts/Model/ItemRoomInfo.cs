﻿using UnityEngine;
using System.Collections;

public class ItemRoomInfo {
    public int ID { get; set; }
    public long Bet { get; set; }
    public int PlayerCurrent { get; set; }
    public int PlayerMax { get; set; }
    public long ChipRequire { get; set; }
    public bool IsPass { get; set; }
    public string Host { get; set; }
    public bool IsPlaying { get; set; }

    public ItemRoomInfo(int currentLevel, int _id, long _bet, long _require, int _playerCount, int _gameId, bool _isPass, string _hostName, bool _isPlaying=false)
    {
        ID = _id;
        Bet = _bet;
        ChipRequire = _require;
        PlayerCurrent = _playerCount;
        IsPass = _isPass;
        IsPlaying = _isPlaying;
        Host = _hostName;
        if (currentLevel == 2)
        {
            PlayerMax = 2;
            return;
        }
        switch (_gameId)
        {
            case 5:
            case 6:
                PlayerMax = 4;
                break;
            case 1:
                PlayerMax = 5;
                break;
            case 2:
                PlayerMax = 4;
                break;
            case 4:
                PlayerMax = 4;
                break;
            case (int)GAMEID.BaiCao:
            case (int)GAMEID.Lieng:
                PlayerMax = 6;
                break;
        }
        
    }

   
}
