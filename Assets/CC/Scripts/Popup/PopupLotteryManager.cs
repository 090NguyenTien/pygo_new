﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Xml;
using System;

public class PopupLotteryManager : MonoBehaviour {
    [SerializeField]
    Button BtnBack;
    [SerializeField]
    Button BtnBetPanel;

    [SerializeField]
    GameObject TOP;
    [SerializeField]
    GameObject MID;
    [SerializeField]
    PopupAlertManager alertManager;

    [SerializeField]
    GameObject BetPanel;
    [SerializeField]
    Text UserName;
    [SerializeField]
    Text UserChip;
    [SerializeField]
    Image avata, BorderAvatar, BorderAvatar_Defaut;
    //[SerializeField]
    //HomeView Home;
	[SerializeField]
	HomeViewV2 HomeV2;
    public GameObject HistoryPanel;
    public GameObject StatisticalPanel;
    public GameObject ResultPanel;

    private ResultLotteryManager Result;
    public BetLotteryManager Bet;
    private StatisticalLotteryManager Statistical;
    public HistoryMannager History;
    public BtnLotteryManager BtnLottery;

    public static string Panel;
    public static bool OnDatCuoc;
    public static bool OnThangThua;

    private GameObject ThongTinDat;
    private GameObject ThongTinThoiGian;
    private Text TxtTimeCountDown;
    private Text TxtTime;
    private Text TxtMinuteCountDown;
    private Text TxtMinute;
    private Text TxtSecondsCountDown;
    
    private Image ImgThongBao;
    private Image ImgBgTime;


    private GameObject ThongTinDat_Top;
    private GameObject ThongTinThoiGian_Top;
    private Text TxtTimeCountDown_Top;
    private Text TxtTime_Top;
    private Text TxtMinuteCountDown_Top;
    private Text TxtMinute_Top;
    private Text TxtSecondsCountDown_Top;

    private Image ImgThongBao_Top;
    private Image ImgBgTime_Top;

    private string StrHTML;

    private long seconds;

    bool IsInited = false;
    [SerializeField]
    TMPro.TextMeshProUGUI txtNameUser_;

    public Dictionary<int, string> DicDate;

    private Dictionary<int, string> DicResult;

    public Dictionary<string, DataResultLottery> DicDataResult;

    private Button BtnQuit;
    public Button BtnBet;

    private string Date_0;
    private string Date_1;
    private string Date_2;
    private string Date_3;
    private string Date_4;
    private string Date_5;
    private string Date_6;

    public bool IsDataReult = false;
    public bool EndBetTime = false;
    private bool OnHistoryInMain = false;

    [SerializeField]
    GameObject ResultNotification;
    private Animator ResultNotification_Anim;
    private bool IsHaveResult;

    public void showPopupLottery()
    {
       
        Init();
    }

    public void OnSFSResponse(GamePacket gp)
    {
        Debug.Log("OnSFSResponse " + gp.cmd);
        switch (gp.cmd)
        {
            case CommandKey.LotteryInit:
                onLotteryInit(gp);
                break;
            case CommandKey.LotteryBet:
                onLotteryBetting(gp);
                break;
            case "1500":
                History.SetDataTenDay(gp);
                break;
            case "AppearNumber":
                Statistical.SetArrayAppearRepones(gp);
                break;
            case "NotAppearNumber":
                Statistical.SetArrayNotAppearRepones(gp);
                break;
            case "HotNumber":
                Statistical.SetArrayHotNumberRepones(gp);
                break;
            case CommandKey.ResultBet:
                Result.SetBetResult(gp);
                History.SetDataBet(gp);
                break;
            case CommandKey.USER_EXIT:
                Result.ClearDicData();
                gameObject.SetActive(false);
                UpdateChipText();              
                break;
        }
    }    

    public void Init()
    {
        //if (HomeController.FistClickBtnLotteryAfterHaveResult == true)
        //{
        //    HomeController.FistClickBtnLotteryAfterHaveResult = false;
        //}
        Panel = "Main";

        BtnQuit = gameObject.transform.GetChild(0).gameObject.transform.GetChild(9).gameObject.GetComponent<Button>();
        BtnQuit.onClick.RemoveAllListeners();
        BtnQuit.onClick.AddListener(BtnHomeLotteryOnClick);

        gameObject.SetActive(true);
        Result = gameObject.GetComponent<ResultLotteryManager>();
        Statistical = gameObject.GetComponent<StatisticalLotteryManager>();

        #region Phần hiển thị thông báo ở TOP

       // UserName.text = MyInfo.NAME;
        txtNameUser_.text = MyInfo.NAME;
        long chip = MyInfo.CHIP;
        
        UserChip.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
        avata.GetComponent<Image>().sprite = MyInfo.sprAvatar;

        int id_Bor = MyInfo.MY_BOR_AVATAR;
        if (id_Bor != -1)
        {
            Sprite h = DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName);
            if (h != null)
            {
                BorderAvatar.sprite = DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName);
                BorderAvatar.gameObject.SetActive(true);
                BorderAvatar_Defaut.gameObject.SetActive(false);
            }
            else
            {
                BorderAvatar.gameObject.SetActive(false);
                BorderAvatar_Defaut.gameObject.SetActive(true);
            }

        }
        else
        {
            BorderAvatar.gameObject.SetActive(false);
            BorderAvatar_Defaut.gameObject.SetActive(true);
        }



        ImgBgTime_Top = TOP.transform.GetChild(11).gameObject.GetComponent<Image>();
        ThongTinThoiGian_Top = ImgBgTime_Top.transform.GetChild(0).gameObject;
        TxtTime_Top = ThongTinThoiGian_Top.transform.GetChild(1).gameObject.GetComponent<Text>();
        TxtMinute_Top = ThongTinThoiGian_Top.transform.GetChild(3).gameObject.GetComponent<Text>();
        TxtTimeCountDown_Top = ThongTinThoiGian_Top.transform.GetChild(0).gameObject.GetComponent<Text>();
        TxtMinuteCountDown_Top = ThongTinThoiGian_Top.transform.GetChild(2).gameObject.GetComponent<Text>();
        TxtSecondsCountDown_Top = ThongTinThoiGian_Top.transform.GetChild(4).gameObject.GetComponent<Text>();

        #endregion

        #region Phần hiển thị thông báo thời gian ở MID
         
        ThongTinDat = MID.transform.GetChild(3).gameObject;
        ImgThongBao = ThongTinDat.transform.GetChild(0).gameObject.GetComponent<Image>();
        ImgBgTime = ThongTinDat.transform.GetChild(1).gameObject.GetComponent<Image>();
        ThongTinThoiGian = ImgBgTime.transform.GetChild(0).gameObject;
        TxtTime = ThongTinThoiGian.transform.GetChild(1).gameObject.GetComponent<Text>();
        TxtMinute = ThongTinThoiGian.transform.GetChild(3).gameObject.GetComponent<Text>();
        TxtTimeCountDown = ThongTinThoiGian.transform.GetChild(0).gameObject.GetComponent<Text>();
        TxtMinuteCountDown = ThongTinThoiGian.transform.GetChild(2).gameObject.GetComponent<Text>();
        TxtSecondsCountDown = ThongTinThoiGian.transform.GetChild(4).gameObject.GetComponent<Text>();

        ThongTinDat.SetActive(false);

        #endregion

        EndBetTime = true;
        BtnBetPanel.gameObject.GetComponent<Image>().color = Color.gray;
        BtnBetPanel.transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.gray;
        OnMID();

        BtnBack.onClick.RemoveAllListeners();
        BtnBack.onClick.AddListener(BtnBackOnClick);

        

        BtnBet = gameObject.transform.GetChild(0).gameObject.transform.GetChild(10).gameObject.GetComponent<Button>();
        BtnBet.onClick.RemoveAllListeners();
        BtnBet.onClick.AddListener(BtnBetLotteryOnClick);
        BtnBet.gameObject.SetActive(false);
        BtnBet.gameObject.GetComponent<Image>().color = Color.gray;

        DicDate = new Dictionary<int, string>();
        DicResult = new Dictionary<int, string>();
        DicDataResult = new Dictionary<string, DataResultLottery>();

        IsHaveResult = false;
        ResultNotification_Anim = ResultNotification.GetComponent<Animator>();
        ResultNotification.SetActive(false);

        Date_0 = "";
        Date_1 = "";
        Date_2 = "";
        Date_3 = "";
        Date_4 = "";
        Date_5 = "";
        Date_6 = "";
       
        OnDatCuoc = false;
        OnThangThua = false;
        GetLotteryInit();
        StartCoroutine(LoadDataResult());
    }
    private void showAlertTimeOutBet()
    {
        AlertController.api.showAlert("Đã hết thời gian đặt cược vui lòng quay lại vào ngày mai");
    }
    private void GetLotteryInit()
    {
        GamePacket gp = new GamePacket(CommandKey.LotteryInit);
        SFS.Instance.SendRoomRequest(gp);
    }

    public void ChangeTextUserChip()
    {
        UserChip.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
    }


    #region Thời gian
    // Nhận dữ liệu thời gian từ sever
    internal void onLotteryInit(GamePacket gp)
    {

        bool isEnable = gp.GetBool(ParamKey.LOTTERY_ENABLE);
        if (isEnable) // Vẫn còn thời gian đặt cược
        {
            long remainTime = gp.GetLong(ParamKey.REMAIN_TIME);

            Debug.LogWarning("remainTime ==== " + remainTime);

            seconds = remainTime / 1000;
            Debug.LogWarning("seconds ==== " + seconds);
            StartCoroutine(CountdownTime());
        }
        else // Đã hết thời gian đặt cược
        {
            ShowCountdownTime(0);
           // seconds = 3600;
           // StartCoroutine(CountdownTime());
        }
    }

    // Đếm ngược thời gian
    private IEnumerator CountdownTime()
    {
        bool fistTime = true;
        while (true)
        {
            yield return new WaitForSeconds(1f);
            --seconds;
            if (fistTime == true && seconds > 0)
            {
                EndBetTime = false;
                BtnBetPanel.gameObject.GetComponent<Image>().color = Color.white;
                BtnBetPanel.transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.white;
                BtnBet.gameObject.GetComponent<Image>().color = Color.white;
                ThongTinDat.SetActive(true);
                ImgBgTime.gameObject.SetActive(true);
                ImgThongBao.sprite = Resources.Load<Sprite>("Icon/TextTime2");
                ImgThongBao.SetNativeSize();

                if (Panel != "Main")
                {
                    ImgBgTime_Top.gameObject.SetActive(true);
                }                  
                fistTime = false;
            }
            ShowCountdownTime((int)seconds);

        }
    }

    // Hiển thị

    private void ShowCountdownTime(int time)
    {
        if (time <= 0) // Trường hợp thời gian đặt cược đã hết
        {
            EndBetTime = true;
            BtnBetPanel.gameObject.GetComponent<Image>().color = Color.gray;
            BtnBetPanel.transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.gray;
            BtnBet.gameObject.GetComponent<Image>().color = Color.gray;
            BtnBet.onClick.RemoveAllListeners();
            BtnBet.onClick.AddListener(showAlertTimeOutBet);
            ThongTinDat.SetActive(true);
            ImgBgTime.gameObject.SetActive(false);
            ImgThongBao.sprite = Resources.Load<Sprite>("Icon/TextTime2");
            ImgThongBao.SetNativeSize();
            RectTransform rectImgThongBao = ImgThongBao.GetComponent<RectTransform>();
            rectImgThongBao.anchoredPosition = new Vector3(-5, -100, 0);
         
            //MID
            TxtTime.gameObject.SetActive(false);
            TxtTimeCountDown.gameObject.SetActive(false);
            TxtSecondsCountDown.gameObject.SetActive(false);
            TxtMinute.gameObject.SetActive(false);
            TxtMinuteCountDown.gameObject.SetActive(false);
            //TOP
            TxtTime_Top.gameObject.SetActive(false);
            TxtTimeCountDown_Top.gameObject.SetActive(false);
            TxtSecondsCountDown_Top.gameObject.SetActive(false);
            TxtMinute_Top.gameObject.SetActive(false);
            TxtMinuteCountDown_Top.gameObject.SetActive(false);
            StopCoroutine(CountdownTime());
        }
        else
        {
            ImgThongBao.sprite = Resources.Load<Sprite>("Icon/TextTime");
            ImgThongBao.SetNativeSize();
            RectTransform rectImgThongBao = ImgThongBao.GetComponent<RectTransform>();
            rectImgThongBao.anchoredPosition = new Vector3(-5, -50, 0);

            // Tính thời gian:
            int gio = time / 3600;
            int phut = (time % 3600) / 60;
            int giay = time - (gio * 3600) - (phut * 60);



            // Hiển thị
            if (time < 60) // Thời gian chỉ còn được tính bằng giây
            {
                //MID 
                RectTransform rectThongTin = ThongTinThoiGian.GetComponent<RectTransform>();
                rectThongTin.anchoredPosition = new Vector3(88.1f, 0, 0);
                TxtSecondsCountDown.gameObject.SetActive(false);
                TxtMinute.gameObject.SetActive(false);
                TxtTime.gameObject.SetActive(false);
                TxtMinute.gameObject.SetActive(false);
                TxtMinuteCountDown.gameObject.SetActive(false);
                //TOP
                RectTransform rectThongTin_Top = ThongTinThoiGian_Top.GetComponent<RectTransform>();
                rectThongTin_Top.anchoredPosition = new Vector3(87.1f, 0, 0);
                TxtSecondsCountDown_Top.gameObject.SetActive(false);
                TxtMinute_Top.gameObject.SetActive(false);
                TxtTime_Top.gameObject.SetActive(false);
                TxtMinute_Top.gameObject.SetActive(false);
                TxtMinuteCountDown_Top.gameObject.SetActive(false);

                if (giay < 10)
                {
                    TxtTimeCountDown.text = "0" + giay.ToString();
                    TxtTimeCountDown_Top.text = "0" + giay.ToString();
                }
                else
                {
                    TxtTimeCountDown.text = giay.ToString();
                    TxtTimeCountDown_Top.text = giay.ToString();
                }
            }
            else if (time < 3600) // Thời gian chỉ còn được tính bằng phút
            {
                //MID
                RectTransform rectThongTin = ThongTinThoiGian.GetComponent<RectTransform>();
                rectThongTin.anchoredPosition = new Vector3(46.1f, 0, 0);
                TxtSecondsCountDown.gameObject.SetActive(false);
                TxtMinute.gameObject.SetActive(false);
                //TOP
                RectTransform rectThongTin_Top = ThongTinThoiGian_Top.GetComponent<RectTransform>();
                rectThongTin_Top.anchoredPosition = new Vector3(48.3f, 0, 0);
                TxtSecondsCountDown_Top.gameObject.SetActive(false);
                TxtMinute_Top.gameObject.SetActive(false);

                if (phut < 10)
                {
                    TxtTimeCountDown.text = "0" + phut.ToString();
                    TxtTimeCountDown_Top.text = "0" + phut.ToString();
                }
                else
                {
                    TxtTimeCountDown.text = phut.ToString();
                    TxtTimeCountDown_Top.text = phut.ToString();
                }

                if (giay < 10)
                {
                    TxtMinuteCountDown.text = "0" + giay.ToString();
                    TxtMinuteCountDown_Top.text = "0" + giay.ToString();
                }
                else
                {
                    TxtMinuteCountDown.text = giay.ToString();
                    TxtMinuteCountDown_Top.text = giay.ToString();
                }
            }
            else // Thời gian tính bằng giờ
            {
                RectTransform rectThongTin_Top = ThongTinThoiGian_Top.GetComponent<RectTransform>();
                rectThongTin_Top.anchoredPosition = new Vector3(2.2f, 0, 0);

                RectTransform rectThongTin = ThongTinThoiGian.GetComponent<RectTransform>();
                rectThongTin.anchoredPosition = new Vector3(2.2f, 0, 0);

                if (gio < 10)
                {
                    TxtTimeCountDown.text = "0" + gio.ToString();
                    TxtTimeCountDown_Top.text = "0" + gio.ToString();
                }
                else
                {
                    TxtTimeCountDown.text = gio.ToString();
                    TxtTimeCountDown_Top.text = gio.ToString();
                }

                if (phut < 10)
                {
                    TxtMinuteCountDown.text = "0" + phut.ToString();
                    TxtMinuteCountDown_Top.text = "0" + phut.ToString();
                }
                else
                {
                    TxtMinuteCountDown.text = phut.ToString();
                    TxtMinuteCountDown_Top.text = phut.ToString();
                }

                if (giay < 10)
                {
                    TxtSecondsCountDown.text = "0" + giay.ToString();
                    TxtSecondsCountDown_Top.text = "0" + giay.ToString();
                }
                else
                {
                    TxtSecondsCountDown.text = giay.ToString();
                    TxtSecondsCountDown_Top.text = giay.ToString();
                }
            }
        }
    }

    #endregion


    #region Kết quả xổ số trong sáu ngày

    // Load kết quả từ https:
    IEnumerator LoadDataResult()
    {
        using (WWW www = new WWW("https://xskt.com.vn/rss-feed/mien-bac-xsmb.rss"))
        {
            yield return www;
            StrHTML = www.text;

            XmlDocument tmp = new XmlDocument();
            tmp.LoadXml(StrHTML);

            XmlElement root = tmp.DocumentElement;

            XmlNodeList descriptionList = root.GetElementsByTagName("description");
            XmlNodeList titleList = root.GetElementsByTagName("title");

            int indexDate = -1;
            int indexResult = -1;

            DicDate.Clear();
            DicResult.Clear();

            // Lấy dữ liệu ngày
            IEnumerator ienumDate = titleList.GetEnumerator();
            while (ienumDate.MoveNext())
            {
                XmlNode title = (XmlNode)ienumDate.Current;
                string ngay = title.InnerText;
                DicDate.Add(indexDate, ngay);
                ++indexDate;
            }

            // Lấy dữ liệu kết quả
            IEnumerator ienumResult = descriptionList.GetEnumerator();
            while (ienumResult.MoveNext())
            {
                XmlNode title = (XmlNode)ienumResult.Current;
                string ketqua = title.InnerText;
                DicResult.Add(indexResult, ketqua);
                ++indexResult;
            }

            // Tổng hợp thành dữ liệu kết quả chính
            Date_0 = DicDate[0];
            Date_1 = DicDate[1];
            Date_2 = DicDate[2];
            Date_3 = DicDate[3];
            Date_4 = DicDate[4];
            Date_5 = DicDate[5];

            DicDataResult.Clear();
            for (int i = 0; i < DicDate.Count - 1; i++)
            {
                SetUpDicDataResult(i);
            }
            IsDataReult = true;
        }
        yield return null;
    }

    // Xử lý dữ liệu
    private void SetUpDicDataResult(int index)
    {
        string DuLieuNgay = DicResult[index];

        string[] Giai = DuLieuNgay.Split('\n');
        string DB = Giai[1].Substring(4, 5);
        string GiaiNhat = Giai[2].Substring(3, 5);
        string GiaiNhi = Giai[3].Substring(3, Giai[3].Length - 3);
        GiaiNhi = GiaiNhi.Replace('-', '|');
        string GiaiBa = Giai[4].Substring(3, Giai[4].Length - 3);
        GiaiBa = GiaiBa.Replace('-', '|');
        string GiaiTu = Giai[5].Substring(3, Giai[5].Length - 3);
        GiaiTu = GiaiTu.Replace('-', '|');
        string GiaiNam = Giai[6].Substring(3, Giai[6].Length - 3);
        GiaiNam = GiaiNam.Replace('-', '|');
        string GiaiSau = Giai[7].Substring(3, Giai[7].Length - 3);
        GiaiSau = GiaiSau.Replace('-', '|');
        string GiaiBay = Giai[8].Substring(3, Giai[8].Length - 3);
        GiaiBay = GiaiBay.Replace('-', '|');

        DataResultLottery Result = new DataResultLottery();

        Result.DacBiet = DB;
        Result.GiaiNhat = GiaiNhat;
        Result.GiaiNhi = GiaiNhi;
        Result.GiaiBa = GiaiBa;
        Result.GiaiTu = GiaiTu;
        Result.GiaiNam = GiaiNam;
        Result.GiaiSau = GiaiSau;
        Result.GiaiBay = GiaiBay;

        string key = "";
        if (index == 0)
        {
            key = Date_0;
        }
        else if (index == 1)
        {
            key = Date_1;
        }
        else if (index == 2)
        {
            key = Date_2;
        }
        else if (index == 3)
        {
            key = Date_3;
        }
        else if (index == 4)
        {
            key = Date_4;
        }
        else if (index == 5)
        {
            key = Date_5;
        }
       if (DicDataResult.ContainsKey(key) == true)
        {
            DicDataResult.Remove(key);
        }
        DicDataResult.Add(key, Result);
    }

    // Truy xuất dữ liệu

    public string GetStringDateResult(int index) // Lấy string Ngày
    {
        string Title = "";
        Title = DicDate[index];
        return Title;
    }


    public DataResultLottery GetDataResultByDateIndex(int Date_) // Lấy kết quả
    {
        string key = "";
        if (Date_ == 0)
        {
            key = Date_0;
        }
        else if (Date_ == 1)
        {
            key = Date_1;
        }
        else if (Date_ == 2)
        {
            key = Date_2;
        }
        else if (Date_ == 3)
        {
            key = Date_3;
        }
        else if (Date_ == 4)
        {
            key = Date_4;
        }
        else if (Date_ == 5)
        {
            key = Date_5;
        }
        DataResultLottery t = DicDataResult[key];
        return t;
    }

    #endregion


    #region Xử lý khi đặt cược
    private void onLotteryBetting(GamePacket gp)
    {
        int grp = gp.GetInt("grs");
        if (grp == 1)
        {
            alertManager.Show("Đặt cược thành công, hãy chờ xem kết quả nhé!", AfterBetSuccess);          
        }
        else if (grp == -1)
        {
            alertManager.Show("Bạn không có đủ tiền cược, vui lòng kiểm tra lại mức cược hoặc nạp thêm chip nhé!", alertManager.Hide);
        }
        else if (grp == -2)
        {
            alertManager.Show("Đặt cược không hợp lệ, vui lòng kiểm tra lại!", alertManager.Hide);
        }
        else if (grp == -3)
        {
            alertManager.Show("Tiền cược tối thiểu là 1000 chip, vui lòng kiểm tra lại", alertManager.Hide);
        }
    }

    private void AfterBetSuccess()
    {
        Bet.OffPanelXacNhan();
        alertManager.Hide();
        Bet.gameObject.SetActive(false);
        BetPanel.SetActive(false);
        OnHistoryButtonClick();;
    }

    #endregion


    #region Button

    public void BtnBackOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        if (Panel == "Main")
        {
            GamePacket gp = new GamePacket(CommandKey.USER_EXIT);
            SFS.Instance.SendRoomRequest(gp);
        }
        else if (Panel == "DatCuoc")
        {
            BetPanel.SetActive(false);
            Panel = "Main";
            OnDatCuoc = false;
            Bet.BackUpInit();
            OnMID();
        }
        else if (Panel == "LichSu")
        {
            History.OffTxtNotification();
            History.CheckShowItemTenDay(false);
            HistoryPanel.SetActive(false);
            if (OnDatCuoc == true)
            {
                BetPanel.SetActive(true);
                Panel = "DatCuoc";
            }

            if (OnHistoryInMain == true)
            {
                OnMID();
                OnHistoryInMain = false;
                Panel = "Main";
            }
            BtnBet.gameObject.SetActive(false);
        }
        else if (Panel == "ThongKe")
        {
            StatisticalPanel.SetActive(false);
            Statistical.BackUpInit();
            Panel = "Main";
            OnMID();
            BtnBet.gameObject.SetActive(false);
        }
        else if (Panel == "KetQua")
        {
            if (OnThangThua == false)
            {

                Result.CheckOnPanelResult();
            }
            else
            {
                ResultPanel.SetActive(false);
                Result.BackUpInit();
                Panel = "Main";
                OnThangThua = true;
                BtnBet.gameObject.SetActive(false);
                OnMID();
            }           
        }
    }


    public void BtnHomeLotteryOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        if (Panel == "DatCuoc")
        {
            BetPanel.SetActive(false);
            OnDatCuoc = false;
            OnMID();
            Bet.BackUpInit();
        }
        else if (Panel == "LichSu")
        {
            History.OffTxtNotification();
            History.CheckShowItemTenDay(false);
            HistoryPanel.SetActive(false);
            if (OnDatCuoc == true)
            {
                BetPanel.SetActive(false);
                Bet.BackUpInit();
            }
            BtnBet.gameObject.SetActive(false);
            OnMID();
        }
        else if (Panel == "ThongKe")
        {
            StatisticalPanel.SetActive(false);
            Statistical.BackUpInit();
            OnMID();
            BtnBet.gameObject.SetActive(false);
        }
        else if (Panel == "KetQua")
        {
            if (OnThangThua == false)
            {
                Result.OnPanelBetResult(true, true);
            }
            else
            {                
                OnThangThua = true;                
            }
            ResultPanel.SetActive(false);
            OnMID();
            Result.BackUpInit();
            BtnBet.gameObject.SetActive(false);
        }
        Panel = "Main";
       // gameObject.SetActive(false);
      //  GamePacket gp = new GamePacket(CommandKey.USER_EXIT);
      //  SFS.Instance.SendRoomRequest(gp);
    }


    public void BtnBetLotteryOnClick()
    {
        if (EndBetTime == false)
        {
            SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
            if (Panel == "LichSu")
            {
                History.OffTxtNotification();
                History.CheckShowItemTenDay(false);
                HistoryPanel.SetActive(false);
                if (OnDatCuoc == true)
                {
                    Bet.showPanelBet();
                }
                else if (OnThangThua == true)
                {
                    ResultPanel.SetActive(false);
                    Result.BackUpInit();
                    Bet.showPanelBet();
                }

                if (OnHistoryInMain == true)
                {
                    OnHistoryInMain = false;
                    Bet.showPanelBet();
                }
            }
            else if (Panel == "ThongKe")
            {
                StatisticalPanel.SetActive(false);
                Statistical.BackUpInit();
                Bet.showPanelBet();
            }
            else if (Panel == "KetQua")
            {
                if (OnThangThua == false)
                {
                    Result.CheckOnPanelResult();
                }
                else
                {
                    ResultPanel.SetActive(false);
                    OnMID();
                }
                ResultPanel.SetActive(false);
                Result.BackUpInit();
                Bet.showPanelBet();
            }
        }
    }

    public void OnHistoryButtonClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        if (Panel == "Main")
        {
            OnHistoryInMain = true;
            OffMID();
        }
        else
        {
            OnHistoryInMain = false;
        }
        if (!HistoryPanel.activeSelf)
            HistoryMannager.Instance.Show();
        else
            HistoryMannager.Instance.Hide();
    }


    public void OnStatisticalButtonClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        OffMID();
        if (!StatisticalPanel.activeSelf)
            StatisticalLotteryManager.Instance.AppearNumber();
        else
            StatisticalLotteryManager.Instance.Hide();
    }

    public void OnResultButtonClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        OffMID();
        if (!ResultPanel.activeSelf)
        {
            ResultLotteryManager.Instance.ShowPanelResult();
        }
    }

    #endregion

    private void UpdateChipText()
    {
        string chip = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
		//if(Home != null)
  //      	Home.UpdateChip(chip);
		if(HomeV2 != null)
			HomeV2.UpdateChip(chip);
    }

	public void OpenShop(){
		//if (Home != null)
		//	Home.ShopOnClick ();
		if(HomeV2 != null)
			HomeV2.ShopOnClick();
	}

	public void OpenSetting(){
		//if (Home != null)
		//	Home.ModuleOnClick ();
		if(HomeV2 != null)
			HomeV2.ModuleOnClick();
	}

    public void OnNotificationResult()
    {
        IsHaveResult = true;
        ResultNotification.SetActive(true);
    }

    public void OffNotificationResult()
    {
        IsHaveResult = false;
        ResultNotification.SetActive(false);
    }

    public void ShowNotificationLottery()
    {
        BtnLottery.OnNotification();
        OnNotificationResult();
    }

    public void NotShowNotificationLottery()
    {
        BtnLottery.OffNotification();
        OffNotificationResult();
    }

    public void OffMID()
    {
        if (EndBetTime == true)
        {
            ImgBgTime_Top.gameObject.SetActive(false);
        }
        else
        {
            ImgBgTime_Top.gameObject.SetActive(true);         
        }
        MID.SetActive(false);
        BtnQuit.gameObject.SetActive(true);
    }

    public void OnMID()
    {
        BtnQuit.gameObject.SetActive(false);
        Debug.Log("toi ne");
        if (EndBetTime == true)
        {        
            ImgBgTime.gameObject.SetActive(false);
        }        
        ImgBgTime_Top.gameObject.SetActive(false);
        MID.SetActive(true);
        
    }

}

// Lottery
public class DataResultLottery
{
    public string DacBiet { get; set; }
    public string GiaiNhat { get; set; }
    public string GiaiNhi { get; set; }
    public string GiaiBa { get; set; }
    public string GiaiTu { get; set; }
    public string GiaiNam { get; set; }
    public string GiaiSau { get; set; }
    public string GiaiBay { get; set; }
}


// Statistical
public class ItemNotAppearNumber
{
    public string Number_1 { get; set; }
    public string Number_2 { get; set; }

}

// Result
public class DataItemResult // Dành cho tờ giấy dò
{
    public string Number_1 { get; set; }
    public string Number_2 { get; set; }
    public string Number_3 { get; set; }
    public string Number_4 { get; set; }
    public string Number_5 { get; set; }
    public string Number_6 { get; set; }
}

public class DataItemBetToDay
{
    public string SoDanh { get; set; }
    public string LoaiDe { get; set; }
    public string TienDat { get; set; }
    public string TienThang { get; set; }
    public string SoLanXuatHien { get; set; }
    public bool Win { get; set; }
}


public class DataBetBeforResult
{
    public int index { get; set; }
    public string SoDanh { get; set; }
    public string LoaiDe { get; set; }
    public string TienCuoc { get; set; }
}

public class DataBetAfterResult
{
    public int index { get; set; }
    public bool iwin { get; set; }
    public string SoDanh { get; set; }
    public string LoaiDe { get; set; }
    public string TienCuoc { get; set; }
    public string TienThang { get; set; }
    public string SolanXuatHien { get; set; }
}
