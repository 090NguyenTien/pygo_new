﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using System.Collections.Generic;
using System;
using Sfs2X.Entities.Data;

public class PopupMissionManager : MonoBehaviour {

    [SerializeField]
    GameObject panelPopupMissionManager, objItemMission;
    [SerializeField]
    PopupAlertManager popupAlertManager;
    [SerializeField]
    PopupReceiveGiftMission panelReceiveGiftMission;
    [SerializeField]
    Transform trsfItemParent;
    [SerializeField]
    Button btnClose;
    private SFS sfs
    {
        get { return SFS.Instance; }
    }

    public GameObject PanelPopupMissionManager
    {
        get
        {
            return panelPopupMissionManager;
        }

        set
        {
            panelPopupMissionManager = value;
        }
    }

    //onCallBack _closeOnClick;

    List<ItemMissionRenderer> lstItemMission;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnEnable()
    {
        Show();
        Init();
       // if (HomeViewV2.ParticleLogo) HomeViewV2.ParticleLogo.Stop();
    }
    public void Init()
    {
        if (lstItemMission ==null) lstItemMission = new List<ItemMissionRenderer>();
        btnClose.onClick.AddListener(CloseOnClick);
        panelReceiveGiftMission.Hide();
    }
    void CloseOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        Hide();
        Destroy(gameObject);
        //if (HomeViewV2.ParticleLogo) HomeViewV2.ParticleLogo.Play();
    }

    //void ItemChildOnClick(int _id)
    //{
    //    SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

    //    int index = lstId.IndexOf(_id);
    //    //		Debug.Log (index);
    //    if (index > -1 && index < trsfItemParent.childCount)
    //    {
    //        Destroy(trsfItemParent.GetChild(index).gameObject);
    //        lstId.Remove(_id);
    //    }

    //    GameObject.FindGameObjectWithTag("GameController").SendMessage("ItemPlayerInviteOnClick", _id);
    //}

    public void Show()
    {
        //if(LoadingManager.Instance) LoadingManager.Instance.ENABLE = true;       
        panelReceiveGiftMission.Hide();
        GamePacket gp = new GamePacket(CommandKey.MISSION);
        SFS.Instance.GetMissionRequest(gp);

        //test Show List Mission Client
       // string jsondata = JsonUtility.ToJson('{ "level":1,"timeElapsed":47.5,"playerName":"Dr Charles Francis"}');
            //('[{"name":"danh thang 100 van","curent":10,"require":100,"gift":{ "chip":1000000,"circle":1} },{ "name":"Thang 10 ty chip","curent":1000000000,"require":10000000000,"gift":{ "chip":1000000,"circle":1} }]');

    }
    public void Hide()
    {
        ClearAllitem();
        btnClose.onClick.RemoveListener(CloseOnClick);
        PanelPopupMissionManager.SetActive(false);
    }
    public void OnSFSResponse(GamePacket param)
    {
        Debug.Log("PopupMissionManager - CMD _ " + param.cmd + " - " + param.param.ToJson());
        Debug.Log("PopupMissionManager - CMD _ " + param.cmd + " - " + param);
        if (LoadingManager.Instance) LoadingManager.Instance.ENABLE = false;
        switch (param.cmd)
        {
            case CommandKey.MISSION:
                missonRes(param);
                break;
            case CommandKey.CLAIM_MISSION:
                claimMissonRes(param);
                break;
        }
    }
    private void claimMissonRes(GamePacket param)
    {
        int result = param.GetInt("result");
        //Debug.LogError("claimMissonRes==========result=======" + result);
        if (result == 1)//success
        {
            string id = param.GetString("id");
            ItemMissionRenderer item;
            for (int i = 0; i < lstItemMission.Count; i++)
            {
                item = lstItemMission[i];
                if(item.Id == id)
                {
                    //show qua` nhan duoc
                    //panelReceiveGiftMission.Show();
                    item.CompleteReceiveGift();
                }
            }

        }else//error
        {
            if (result == -1)       AlertController.api.showAlert("Nhiệm Vụ Chưa Hoàn Thành"); //popupAlertManager.Show("Nhiệm Vụ Chưa Hoàn Thành",onCallBackPopupAlert);
            else if (result == -2)  AlertController.api.showAlert("Nhiệm Vụ Không Tồn Tại");//popupAlertManager.Show("Nhiệm Vụ Không Tồn Tại",onCallBackPopupAlert);
            else if (result == -3)  AlertController.api.showAlert("Đã Nhận Thưởng Rồi");//popupAlertManager.Show("Đã Nhận Thưởng Rồi",onCallBackPopupAlert);
            else                    AlertController.api.showAlert("Không Thể Nhận Thưởng");//popupAlertManager.Show("Không Thể Nhận Thưởng", onCallBackPopupAlert);

        }
        
    }

    private void onCallBackPopupAlert()
    {
        popupAlertManager.Hide();
    }

    private void missonRes(GamePacket param)
    {
        ISFSArray aMission = param.GetSFSArray(ParamKey.RESULT);
        ItemMissionRenderer item;
        SFSObject itemMission;
        Debug.Log("PopupMissionManager:::aMission.Size()=====" + aMission.Size());
        
        for (int i = 0; i < aMission.Size(); i++)
        {
            itemMission = (SFSObject)aMission.GetSFSObject(i);
            //if (i < trsfItemParent.childCount)
            //    item = trsfItemParent.GetChild(i).GetComponent<ItemMissionRenderer>();
           // else
           // {
                GameObject obj = Instantiate(objItemMission) as GameObject;
                obj.transform.SetParent(trsfItemParent);
                obj.transform.localScale = Vector3.one;

                item = obj.GetComponent<ItemMissionRenderer>();
            //}
            item.Init(itemMission);
            lstItemMission.Add(item);
        }
        
    }

   

    public void ClearAllitem(int _indexFrom = 0)
    {
        //		Debug.Log (_indexFrom + " - ");
        for (int i = _indexFrom; i < trsfItemParent.childCount; i++)
        {
            GameObject.Destroy(trsfItemParent.GetChild(i).gameObject);
        }
    }
}
