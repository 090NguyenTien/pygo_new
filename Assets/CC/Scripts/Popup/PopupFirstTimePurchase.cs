﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupFirstTimePurchase : MonoBehaviour
{
    //[SerializeField]
    //private HomeView homeview = null;
	[SerializeField]
	private HomeViewV2 homeviewv2 = null;
    public void purchaseNow()
    {
        closePopup();
		//if(homeview != null)
  //      	homeview.ShopOnClick();
		if(homeviewv2 != null)
			homeviewv2.ShopOnClick();
    }

    public void closePopup()
    {
        gameObject.SetActive(false);
    }

}
