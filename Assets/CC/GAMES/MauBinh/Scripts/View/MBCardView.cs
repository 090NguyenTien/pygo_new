﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;
using UnityEngine.UI;
using ControllerMB;

namespace ViewMB {
	
	public class MBCardView : MonoBehaviour {

		MBInGameController Controller;

		public delegate void CallBack ();
		CallBack dealCardsDone, resultRow;

		[SerializeField]
		private GameObject BigCard;
		[SerializeField]
		private List<GameObject> lstHandCardPlayers;
		[SerializeField]
		private List<MBBigCardItem> lstBigCards;
		[SerializeField]
		List<GameObject> lstBackCards;	//Bai up khi dealcards
		[SerializeField]
		private List<MBCardItem> lstCardUser,
			lstCardP1, lstCardP2, lstCardP3;

		//txtResult 4 players
		[SerializeField]
		List<GameObject> lstResultRow, lstStateSorted, lstStateSorting, lstDie;
		[SerializeField]
		private List<Text> lstTxtResultRow, //Hien thi an/thua bao nhieu chi
					lstTxtResultCash;	//Hien thi an/thua bao nhieu tien

		//BIG CARDS
		[SerializeField]
        private Image imgValueBigCardRow1, imgValueBigCardRow2, imgValueBigCardRow3,
            imgBigCardRowSpecial, //3 thung, 3 sanh
            imgBigCardWin,  //sanh rong, luc phe bon, ...
            imgBigCardFail; //Binh lung

        [SerializeField]
        List<Transform> lstRowP0, lstRowP1, lstRowP2, lstRowP3;

        [SerializeField]
		private Sprite[] arrSprSmallSuite, arrSprBigSuite, arrSprValue, arrSprJQK;


		List<RectTransform> lstRectTrsfBigCards;	//Tham chieu toi RectTrsf BigCards
		List<Vector2> lstPosBigCards;	//Giu lai localPosition dau tien

		Dictionary<int, List<MBCardItem>> dictPosHandCards;	//<posClient, cards>

		const float TIME_MOTION = .3f;
		const float ALPHA_CARD = .05f;

		Vector3 sizeSmall = new Vector3 (.8f, .8f, 1);
		Vector3 sizeBig = Vector3.one;

		/// <summary>
		/// Khoi tao duy nhat 1 lan khi Scene Active
		/// </summary>
		public void Init(MBInGameController _controller)
		{
			Controller = _controller;

			dictPosHandCards = new Dictionary<int, List<MBCardItem>> ();
			dictPosHandCards [0] = lstCardUser;
			dictPosHandCards [1] = lstCardP1;
			dictPosHandCards [2] = lstCardP2;
			dictPosHandCards [3] = lstCardP3;


			lstRectTrsfBigCards = new List<RectTransform> ();
            lstPosBigCards = new List<Vector2>();

            for (int i = 0; i < 13; i++)
            {
                lstBigCards[i].Init(i);
                lstRectTrsfBigCards.Add(lstBigCards[i].GetComponent<RectTransform>());
                lstPosBigCards.Add((Vector2)lstRectTrsfBigCards[i].localPosition);
                //Debug.Log (lstPosBigCards [i]);
            }

        }

		#region BIG CARD

		public void ResetAllBigCards()
		{
			for (int i = 0; i < 13; i++) {
				lstBigCards [i].GetComponent<Image> ().raycastTarget = true;
				lstRectTrsfBigCards [i].localPosition = lstPosBigCards [i];
			}
		}
		public void ShowBigCard(int _index, int _idCard)
		{
			ShowCard ((MBCardItem)lstBigCards [_index], _idCard);
		}
		/// <summary>
		/// Hien thi 13 la bai lon cua User 
		/// </summary>
		public void ShowBigCards(int[] _idCards)
		{
			for (int i = 0; i < _idCards.Length; i++)
            {
				ShowCard ((MBCardItem)lstBigCards [i], _idCards [i]);
			}
		}
		public void SetEnableBigCard(bool _enable)
		{
			BigCard.SetActive (_enable);
		}

		public void EndDragFail(int _indexCard)
		{

			SoundManager.PlaySound (SoundManager.BUTTON_CLICK);
			lstRectTrsfBigCards [_indexCard].DOLocalMove (
								lstPosBigCards [_indexCard], TIME_MOTION);
		}
		public void EndDragSuccess(List<int> _lstIndexCards)
		{
			SoundManager.PlaySound (SoundManager.BUTTON_CLICK);
			Controller.DragCardSuccess (_lstIndexCards [0], _lstIndexCards [1]);
		}

		public void ChangeBigCards(int _indexFirst, int _indexSecond, bool excuteAnim = true)
		{
            if (excuteAnim == true)
            {
                //Bay tu vi tri la bai thu 2 || Bay tu vi tri hien tai
                lstRectTrsfBigCards[_indexFirst].transform.parent.SetAsFirstSibling();
                lstRectTrsfBigCards[_indexFirst].SetAsFirstSibling();

                lstRectTrsfBigCards[_indexSecond].position = lstRectTrsfBigCards[_indexFirst].position;
                lstRectTrsfBigCards[_indexSecond].DOLocalMove(lstPosBigCards[_indexSecond], TIME_MOTION);

                lstRectTrsfBigCards[_indexFirst].localPosition = lstPosBigCards[_indexSecond];
                lstRectTrsfBigCards[_indexFirst].DOLocalMove(lstPosBigCards[_indexFirst], TIME_MOTION);
            }
            else
            {
                //Bay tu vi tri la bai thu 2 || Bay tu vi tri hien tai
                lstRectTrsfBigCards[_indexFirst].transform.parent.SetAsFirstSibling();
                lstRectTrsfBigCards[_indexFirst].SetAsFirstSibling();

                lstRectTrsfBigCards[_indexSecond].position = lstRectTrsfBigCards[_indexFirst].position;
                lstRectTrsfBigCards[_indexSecond].anchoredPosition =  lstPosBigCards[_indexSecond];

                lstRectTrsfBigCards[_indexFirst].localPosition = lstPosBigCards[_indexSecond];
                lstRectTrsfBigCards[_indexFirst].anchoredPosition = lstPosBigCards[_indexFirst];
            }
			
		}

        public void sortBigCard(int oldIndexFirst, int oldIndexSecond)
        {
            //sort first card
            List<int> lstIndex = new List<int>();
            int firstPhase = getPhaseByIndex(oldIndexSecond, ref lstIndex);

            List<int> lstSecondIndex = new List<int>();
            int secondPhase = getPhaseByIndex(oldIndexFirst, ref lstSecondIndex);

            if (firstPhase == secondPhase)//cung tren 1 chi
            {
                return;
            }
            for (int i = 0; i < lstIndex.Count ; i++)
            {
                for (int k = lstIndex.Count - 1; k > i; k--)
                {
                    if (lstBigCards[lstIndex[k]].card.Value < lstBigCards[lstIndex[i]].card.Value )
                    {
                        Debug.LogWarning("--------swap card " + lstBigCards[lstIndex[k]].card.Value + "  voi card "  + lstBigCards[lstIndex[i]].card.Value);
                        Controller.DragCardSuccess(lstIndex[k], lstIndex[i], false, false);
                    }
                }
            }

            for (int i = 0; i < lstSecondIndex.Count; i++)
            {
                for (int k = lstSecondIndex.Count - 1; k > i; k--)
                {
                    if (lstBigCards[lstSecondIndex[k]].card.Value < lstBigCards[lstSecondIndex[i]].card.Value)
                    {
                        Controller.DragCardSuccess(lstIndex[k], lstIndex[i], false, false);
                    }
                }
            }
            //for (int i = 0; i < lstSecondIndex.Count; i++)
            //{
            //    if (lstBigCards[oldIndexSecond].card.Value < lstBigCards[lstSecondIndex[i]].card.Value || (lstSecondIndex[i] == lstSecondIndex[lstSecondIndex.Count - 1] && oldIndexSecond != lstSecondIndex[i]))
            //    {
            //        Controller.DragCardSuccess(oldIndexFirst, lstSecondIndex[i], false, false);
            //        break;
            //    }
            //}

        }

        public int getPhaseByIndex(int index, ref List<int> listIndex)
        {
            if (index >= 10)
            {
                listIndex.Add(10);
                listIndex.Add(11);
                listIndex.Add(12);
                return 3;
            }
            else if (index >= 5)
            {
                listIndex.Add(5);
                listIndex.Add(6);
                listIndex.Add(7);
                listIndex.Add(8);
                listIndex.Add(9);
                return 2;
            }
            else if (index >= 0)
            {
                listIndex.Add(0);
                listIndex.Add(1);
                listIndex.Add(2);
                listIndex.Add(3);
                listIndex.Add(4);
                return 1;
            }
            return -1;
        }


		#region Value Rows - Big Cards

		public void SetEnableValueRows(bool _enable)
		{
						imgValueBigCardRow1.gameObject.SetActive (_enable);
						imgValueBigCardRow2.gameObject.SetActive (_enable);
						imgValueBigCardRow3.gameObject.SetActive (_enable);
		}
		public void ShowValueBigCards(MBType _row1, MBType _row2, MBType _row3)
		{
						imgValueBigCardRow1.sprite = MBDataHelper.instance.GetSprRow (_row1);
						imgValueBigCardRow2.sprite = MBDataHelper.instance.GetSprRow (_row2);
						imgValueBigCardRow3.sprite = MBDataHelper.instance.GetSprRow (_row3);
		}

		#endregion

		#region Rows Special - 3 thung, 3 sanh

		public void SetEnableRowsSpecial(bool _enable)
		{
						imgBigCardRowSpecial.gameObject.SetActive (_enable);
		}
		public void ShowRowsSpecial(MBTypeImmediate _type)
		{
						imgBigCardRowSpecial.sprite = MBDataHelper.instance.GetSprWin (_type);
		}

		#endregion

		#region Win special - Sanh rong, Dong hoa, ...

		public void SetEnableBigWin(bool _enable)
		{
						imgBigCardWin.gameObject.SetActive (_enable);
		}
		public void ShowBigWin(MBTypeImmediate _type)
		{
						imgBigCardWin.sprite = MBDataHelper.instance.GetSprWin (_type);
		}

		#endregion

		#region Binh lung

		public void SetEnableValueFail(bool _enable)
		{
						imgBigCardFail.gameObject.SetActive (_enable);
		}

		#endregion

		#endregion

		public void SetOrderRow(int _position, int _row, int _index = 2)
				{
						if (_position == 0)
								lstRowP0 [_row - 1].SetSiblingIndex (_index);
						else if (_position == 1)
								lstRowP1 [_row - 1].SetSiblingIndex (_index); 
						else if (_position == 2)
								lstRowP2 [_row - 1].SetSiblingIndex (_index);
						else if (_position == 3)
								lstRowP3 [_row - 1].SetSiblingIndex (_index);
				}

		#region HandCards

		public void ShowHandCardBigSize()
		{
			for (int i = 0; i < lstHandCardPlayers.Count; i++) {
				lstHandCardPlayers [i].transform.DOScale (sizeBig, TIME_MOTION);
			}
		}
		public void ShowHandCardSmallSize()
        {
			for (int i = 0; i < lstHandCardPlayers.Count; i++)
            {
				lstHandCardPlayers [i].transform.DOScale (sizeSmall, TIME_MOTION);
			}
		}

		public void SetEnableHandCards(int _position, bool _enable)
		{
			lstHandCardPlayers [_position].SetActive (_enable);
		}
	public void SetEnableHandCards(int _position, int _row, bool _enable)
	{
			if (_row == 3) {
					dictPosHandCards [_position] [10].gameObject.SetActive (_enable);
					dictPosHandCards [_position] [11].gameObject.SetActive (_enable);
					dictPosHandCards [_position] [12].gameObject.SetActive (_enable);
			} else if (_row == 2) {
					dictPosHandCards [_position] [5].gameObject.SetActive (_enable);
					dictPosHandCards [_position] [6].gameObject.SetActive (_enable);
					dictPosHandCards [_position] [7].gameObject.SetActive (_enable);
					dictPosHandCards [_position] [8].gameObject.SetActive (_enable);
					dictPosHandCards [_position] [9].gameObject.SetActive (_enable);
			} else if (_row == 1) {
					dictPosHandCards [_position] [0].gameObject.SetActive (_enable);
					dictPosHandCards [_position] [1].gameObject.SetActive (_enable);
					dictPosHandCards [_position] [2].gameObject.SetActive (_enable);
					dictPosHandCards [_position] [3].gameObject.SetActive (_enable);
					dictPosHandCards [_position] [4].gameObject.SetActive (_enable);
			}
	}

		public void ShowHandCardsPlayers(int[] _idCards, int _position)
		{
			for (int i = 0; i < _idCards.Length; i++) {
				ShowCard (dictPosHandCards [_position] [i], _idCards [i]);
			}
		}

		/// <summary>
		/// Hien/An tat ca la bai up cua Players
		/// </summary>
		public void SetEnableBackCard(int _position, bool _enable)
		{
			for (int i = 0; i < 13; i++) {
				dictPosHandCards [_position] [i].BackCard = _enable;
			}
		}
		/// <summary>
		/// Hien/An la bai up theo chi, cua Players
		/// </summary>
		/// <param name="_position">Position.</param>
		public void SetEnableBackCard(int _position, int _row, bool _enable)
		{
			if (_row == 3) {
				dictPosHandCards [_position] [10].BackCard = _enable;
				dictPosHandCards [_position] [11].BackCard = _enable;
				dictPosHandCards [_position] [12].BackCard = _enable;

            } else if (_row == 2) {
				dictPosHandCards [_position] [5].BackCard = _enable;
				dictPosHandCards [_position] [6].BackCard = _enable;
				dictPosHandCards [_position] [7].BackCard = _enable;
				dictPosHandCards [_position] [8].BackCard = _enable;
				dictPosHandCards [_position] [9].BackCard = _enable;
			} else if (_row == 1) {
				dictPosHandCards [_position] [0].BackCard = _enable;
				dictPosHandCards [_position] [1].BackCard = _enable;
				dictPosHandCards [_position] [2].BackCard = _enable;
				dictPosHandCards [_position] [3].BackCard = _enable;
				dictPosHandCards [_position] [4].BackCard = _enable;
			}
		}

        public void setCardLighting(int _position, int _row, bool _enable)//show/hide card toi
        {
            _enable = !_enable;//dao nguoc lai! show light => tat gameobject den
            if (_row == 3)
            {
                dictPosHandCards[_position][10].backCardBackground = _enable;
                dictPosHandCards[_position][11].backCardBackground = _enable;
                dictPosHandCards[_position][12].backCardBackground = _enable;

            }
            else if (_row == 2)
            {
                dictPosHandCards[_position][5].backCardBackground = _enable;
                dictPosHandCards[_position][6].backCardBackground = _enable;
                dictPosHandCards[_position][7].backCardBackground = _enable;
                dictPosHandCards[_position][8].backCardBackground = _enable;
                dictPosHandCards[_position][9].backCardBackground = _enable;
            }
            else if (_row == 1)
            {
                dictPosHandCards[_position][0].backCardBackground = _enable;
                dictPosHandCards[_position][1].backCardBackground = _enable;
                dictPosHandCards[_position][2].backCardBackground = _enable;
                dictPosHandCards[_position][3].backCardBackground = _enable;
                dictPosHandCards[_position][4].backCardBackground = _enable;
            }
        }

        public void ShowAlphaCard(int _position, int _index, bool _isBlur)
		{
	//		Debug.Log(_position + " - " + _index + " _ " + _isBlur);
			dictPosHandCards [_position] [_index].GetComponent<CanvasGroup> ().alpha = 
				_isBlur ? ALPHA_CARD : 1;
		}
		#region So chi

		/// <summary>
		/// Show chi MauBinh (an trang)
		/// </summary>
		public void ShowValueWinAll(string[] _handCardsPlayers, CallBack _onComplete, float _timeComplete = 0)
		{
			//Duyet xem co player nao co MauBinh hay ko?
			for (int i = 0; i < _handCardsPlayers.Length; i++) {
				if (!string.IsNullOrEmpty (_handCardsPlayers [i])) {

					string[] _handCards = _handCardsPlayers [i].Split ('#');

					ShowHandCardsPlayers (ConvertArray(_handCardsPlayers), i);
				}
			}

			StartCoroutine (CallBackThread (_timeComplete, _onComplete));
		}
		/// <summary>
		/// Show 1 chi cua 1 player
		/// </summary>
		public void ShowValueRowAll(int _position, int _row, int[] _handCards)
		{
			if (_handCards == null) {
				Debug.Log ("HANDCARDS null");
				return;
			}

			ShowAlphaRow (_position, _row);
	//		ShowHandCardsPlayers (_handCards, _position);
		}

		/// <summary>
		/// On/Off ket qua 1 chi
		/// </summary>
		public void SetEnableResultRow(int _position, bool _enable)
		{
			lstResultRow [_position].SetActive (_enable);
		}
		/// <summary>
		/// Shows the result a row.
		/// </summary>
		void ShowResultRow(int _position, int _row, int _resultRow)
		{
			ShowAlphaRow (_position, _row);
			ShowResultCash (_position, _resultRow);
		}

		public void ShowAlphaRow(int _position, int _row = 0)
		{	
			if (_row == 0) {
				lstHandCardPlayers [_position].transform.GetChild (0).
				GetComponent<CanvasGroup> ().alpha = 1;

				lstHandCardPlayers [_position].transform.GetChild (1).
				GetComponent<CanvasGroup> ().alpha = 1;

				lstHandCardPlayers [_position].transform.GetChild (2).
				GetComponent<CanvasGroup> ().alpha = 1;
			} else {
				lstHandCardPlayers [_position].transform.GetChild (0).
				GetComponent<CanvasGroup> ().alpha = _row == 3 ? 1 : ALPHA_CARD;

				lstHandCardPlayers [_position].transform.GetChild (1).
				GetComponent<CanvasGroup> ().alpha = _row == 2 ? 1 : ALPHA_CARD;

				lstHandCardPlayers [_position].transform.GetChild (2).
				GetComponent<CanvasGroup> ().alpha = _row == 1 ? 1 : ALPHA_CARD;
			}
		}


		void ShowResultCash(int _position, int _resultRow)
		{
			if (_resultRow > 0) {
				lstTxtResultRow [_position].text = "+" +_resultRow.ToString () + " chi";
			} else if (_resultRow < 0) {
				lstTxtResultRow [_position].text = _resultRow.ToString () + " chi";
			} else
				lstTxtResultRow [_position].text = "+0 chi";
				
		}

		IEnumerator CallBackThread(float _timeDelay, CallBack _callBack)
		{
			yield return new WaitForSeconds (_timeDelay);
			_callBack ();
		}

		#endregion

		#region RESULT CASH

		public void ShowResultCashRow(int _position, long _cash, float _time = 4)
		{
			//Text txtCash = lstTxtResultCash [_position];
			//txtCash.transform.localPosition = Vector3.zero;
			//txtCash.gameObject.SetActive (true);

			//if (_cash >= 0) {
			//	txtCash.text = "+" + Utilities.GetStringMoneyByLong (_cash);
			//					txtCash.color = Color.yellow;
			//} else {
			//	txtCash.text = Utilities.GetStringMoneyByLong (_cash);
			//	txtCash.color = Color.gray;
			//}

			//Sequence sequence = DOTween.Sequence ();
			//sequence.Append (txtCash.transform.DOLocalMoveY (70, _time));
			//sequence.Join (txtCash.DOFade (.5f, _time).SetDelay(_time/2));

			//sequence.AppendCallback (() => {
			//	HideResultCashRow(_position);
			//});
		}
		public void HideResultCashRow(int _position)
		{
			lstTxtResultCash [_position].gameObject.SetActive (false);
		}

		#endregion

		#region RESULT SamHam

		public void SetEnableDie(int _position, bool _enable)
		{
			lstDie [_position].SetActive (_enable);
		}

		#endregion

		#endregion

		#region SortState

		public void SetEnableSorted(int _position, bool _enable)
		{
			if (_position < 1 || _position > 3)
				return;
			lstStateSorted [_position].SetActive (_enable);
		}
		public void SetEnableSorting(int _position, bool _enable)
		{
			if (_position < 1 || _position > 3)
				return;
			lstStateSorting [_position].SetActive (_enable);
		}

		#endregion

		#region Utils

		/// <summary>
		/// Hien thi la bai theo id
		/// </summary>
		private void ShowCard(MBCardItem _cardItem, int _id)
		{

			_cardItem.card = BaseCardInfo.Get (_id);

			_cardItem.Value.sprite = arrSprValue [(_cardItem.card.Value - 1) % 13];

			_cardItem.Value.color = (int)_cardItem.card.Type < 2 ? Color.black : Color.red;

	//		Debug.Log ("TYPE _ " + (int)_cardItem.card.Type);
			_cardItem.SmallSuite.sprite = arrSprSmallSuite [(int)_cardItem.card.Type];

			if (_cardItem.card.Value < 11 || _cardItem.card.Value > 13)
            {
				_cardItem.BigSuite.sprite = arrSprBigSuite [(int)_cardItem.card.Type];
			}
            else
            {
				_cardItem.BigSuite.sprite = arrSprJQK [(_id / 13) * 3 + (_cardItem.card.Value % 11)];
			}
		}

		#endregion

		#region DEAL CARD

		public void SetEnableCardDeal(bool _enable)
		{
			for (int i = 0; i < lstBackCards.Count; i++) {
				lstBackCards [i].SetActive (_enable);

				if (_enable)
					lstBackCards [i].transform.localPosition = Vector3.zero;
			}
		}

		/// <summary>
		/// DealCards
		/// </summary>
		/// <returns>The card.</returns>
		/// <param name="players">vi tri cac player tham gia</param>
		/// <param name="playerCards">ID bai tren tay User</param>
		/// <param name="_onComplete">On complete.</param>
		public IEnumerator DealCard(MBUserInfo[] players, CallBack _onComplete)
		{
			SoundManager.PlaySound (SoundManager.DEAL_MANY_CARD);
			//Create deal cards animation for all players
			dealCardsDone = _onComplete;

			int _count = 52 - 13 * players.Length;

            for (int i = 0; i < 13; i++)
            {
                for (int j = 0; j < players.Length; j++)
                {
                    if (players[j] != null)
                    {
                        if (players[j].IsReady)
                        {
                            GameObject temp = lstBackCards[_count];
                            temp.SetActive(true);
                            temp.transform.localPosition = Vector3.zero;

                            Vector3 pos = lstHandCardPlayers[players[j].ClientPos].transform.position;
                            temp.transform.DOMove(pos, .4f).SetEase(Ease.Linear);

                            _count++;
                            yield return new WaitForSeconds(0.01f);
                        }
                    }
                }
            }
            yield return new WaitForSeconds (1f);

			dealCardsDone ();
		}

		#endregion

		int[] ConvertArray(string[] _arr)
		{
			return Array.ConvertAll<string, int> (_arr, int.Parse);
		}
	}
}