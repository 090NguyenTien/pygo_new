﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TaiXiuBetChipManager : MonoBehaviour {

    public TaiXiuBetChip betChipObject;
    public Sprite[] sprites;

    public Sprite[] spriteEnemy;

    public bool isBotBet = false;

    public TaiXiuBetChip CreateBetChip(int index, string text, bool isBot = false)
    {
        TaiXiuBetChip betChip = Instantiate(betChipObject);
        betChip.isBot = isBot;
        if (isBot == false)
        {
            betChip.Image.sprite = sprites[index];
        }
        else
        {
            betChip.Image.sprite = spriteEnemy[index];
        }
        
        betChip.Text.text = text;
        return betChip;
    }
}
