﻿using UnityEngine;
using System.Collections;

public class DiceSprites : MonoBehaviour {

    private static DiceSprites instance;
    public static DiceSprites Instance { get { return instance; } }
    public Sprite[] Sprites;

    void Awake()
    {
        instance = this;
    }

    public Sprite Get(int index)
    {
        return Sprites[index];
    }
}
