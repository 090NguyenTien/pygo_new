﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine.UI;
using System.Diagnostics;
using DG.Tweening;

public class PhomPlayerSlot : PhomSlot {

	const float PlusSizeX = 3;
	const float PlusSizeY = 190;
    #region Properties

    public GameObject[] MeldBorders;
    public GameObject ButtonsZone;

    [HideInInspector]
    public List<PhomCard> CurrentSelectedCards;
    [HideInInspector]
    public List<SentGroup> SentGroups;

    #endregion

    void Awake()
	{
        SfsId = PhomConst.INVALID_ID;
        SentGroups = new List<SentGroup>();
        LogoBang.gameObject.SetActive(false);
    }

	#region Process Methods

	public void ProcessEnemyGoFirst(string strCombination, PhomDeck deck)
	{
		Combination = PhomUtil.ToCombination(strCombination);
	}

    public override void Reset()
    {
        base.Reset();
        Combination = new Combination();
        CurrentSelectedCards = new List<PhomCard>();
        SentGroups = new List<SentGroup>();
        HideMeldBorders();
    }

    #endregion

    #region Animate Methods

    public void HideMeldBorders()
    {
        MeldBorders[0].SetActive(false);
        MeldBorders[1].SetActive(false);
        MeldBorders[2].SetActive(false);
    }

    public void AnimateGetDealedCards(PhomDeck deck)
    {
        foreach (PhomCard card in Combination.GetSortedCards())
        {
            int id = card.Id;
            deck.Cards[id].transform.SetParent(HandZone.transform);
            deck.Cards[id].transform.localPosition = Vector3.zero;
        }
    }

    public void AnimateSortHandZone()
    {
        HideMeldBorders();
        PhomUtil.ResetCardsState(Combination.GetSortedCards());

        foreach (PhomCard card in Combination.GetSortedCards())
            foreach (Transform transform in HandZone.transform)
                if (transform.name.Contains("Card") && transform.GetComponent<PhomCard>().Id == card.Id)
                    transform.SetAsLastSibling();

        foreach (Transform transform in HandZone.transform)
            foreach (PhomCard eatenCard in EatenCards)
                if (transform.name.Contains("Card") && transform.GetComponent<PhomCard>().Id == eatenCard.Id)
                    transform.GetChild(PhomConst.CARD_MASK_INDEX).gameObject.GetComponent<Image>().color = PhomColor.EatenCardColor;

        int currentMeldOrder = PhomConst.INVALID_INDEX;
        int meldBorder1StartPosition = PhomConst.INVALID_INDEX;
        int meldBorder2StartPosition = PhomConst.INVALID_INDEX;
        int meldBorder3StartPosition = PhomConst.INVALID_INDEX;
        int nonMeldStartPosition = 0;
        
        for (int i = 0; i < HandZone.transform.childCount - 3; i++)
        {
            PhomCard card = HandZone.transform.GetChild(i + 3).GetComponent<PhomCard>();
            card.transform.localScale = new Vector3(1, 1, 1);
            int meldOrder = PhomUtil.GetMeldOrder(HandZone.transform.GetChild(i + 3).GetComponent<PhomCard>().Id, Combination);

            if (meldOrder != currentMeldOrder)
            {
                switch (meldOrder)
                {
                    case 0:
                        meldBorder1StartPosition = i;
                        break;
                    case 1:
                        meldBorder2StartPosition = i;
                        break;
                    case 2:
                        meldBorder3StartPosition = i;
                        break;
                    default:
                        nonMeldStartPosition = i;
                        break;
                }

                currentMeldOrder = meldOrder;
            }
        }

        float distance = 126f * Screen.width / 1600;
        Vector3 center = HandZone.transform.position;
        float x = center.x - (HandZone.transform.childCount - 3) / 2 * distance;

        if (HandZone.transform.childCount % 2 == 1)
            x += distance / 2;

        PhomRoomController.Instance.IsAutoPickingCard = true;

        foreach (Transform t in HandZone.transform)
        {
            if (t.name.Contains("Card"))
            {
                Tweener tn = t.DOMove(new Vector3(x, center.y), 0.1f, false).SetEase(Ease.Linear);
                x += distance;
                tn.OnComplete(PhomRoomController.Instance.AllowPickCard);
            }
        }

        if (HandZone.transform.childCount < 6)
            return;

        float meldBorder1Length = 0f;
        float meldBorder2Length = 0f;
        float meldBorder3Length = 0f;

        if (meldBorder1StartPosition != -1)
            if (meldBorder2StartPosition != -1)
                meldBorder1Length = meldBorder2StartPosition - meldBorder1StartPosition;
            else
                meldBorder1Length = nonMeldStartPosition - meldBorder1StartPosition;

        if (meldBorder2StartPosition != -1)
            if (meldBorder3StartPosition != -1)
                meldBorder2Length = meldBorder3StartPosition - meldBorder2StartPosition;
            else
                meldBorder2Length = nonMeldStartPosition - meldBorder2StartPosition;

        if (meldBorder3StartPosition != -1)
            if (nonMeldStartPosition != 0)
                meldBorder3Length = nonMeldStartPosition - meldBorder3StartPosition;
            else
                meldBorder3Length = HandZone.transform.childCount - 3 - meldBorder3StartPosition;

        float cardWidth = 126f;
        
        if (meldBorder1StartPosition != PhomConst.INVALID_INDEX)
        {
            Vector3 position = HandZone.transform.position;

            position.x -= (HandZone.transform.childCount - 3) / 2 * distance;

            if (HandZone.transform.childCount % 2 == 1)
                position.x += distance / 2;

            position.x += meldBorder1Length / 2 * distance - distance / 2;
            MeldBorders[0].transform.position = position;
			MeldBorders[0].GetComponent<RectTransform>().sizeDelta = new Vector2(meldBorder1Length * cardWidth + PlusSizeX, PlusSizeY);
            MeldBorders[0].SetActive(true);
        }

        if (meldBorder2StartPosition != PhomConst.INVALID_INDEX)
        {
            Vector3 position = HandZone.transform.position;
            position.x -= (HandZone.transform.childCount - 3) / 2 * distance;

            if (HandZone.transform.childCount % 2 == 1)
                position.x += distance / 2;

            position.x += meldBorder1Length * distance - distance / 2;
            position.x += meldBorder2Length / 2 * distance;
            MeldBorders[1].transform.position = position;
			MeldBorders[1].GetComponent<RectTransform>().sizeDelta = new Vector2(meldBorder2Length * cardWidth + PlusSizeX, PlusSizeY);
            MeldBorders[1].SetActive(true);
        }

        if (meldBorder3StartPosition != PhomConst.INVALID_INDEX)
        {
            Vector3 position = HandZone.transform.position;
            position.x -= (HandZone.transform.childCount - 3) / 2 * distance;

            if (HandZone.transform.childCount % 2 == 1)
                position.x += distance / 2;

            position.x += (meldBorder1Length + meldBorder2Length) * distance;
            position.x += meldBorder3Length / 2 * distance - distance / 2;
            MeldBorders[2].transform.position = position;
			MeldBorders[2].GetComponent<RectTransform>().sizeDelta = new Vector2(meldBorder3Length * cardWidth + PlusSizeX, PlusSizeY);
            MeldBorders[2].SetActive(true);
        }
    }

    public new void AnimateSortDiscardZone()
    {
        PhomUtil.ResetCardsState(DiscardedCards);
        base.AnimateSortDiscardZone();
    }

    public void AnimateSortLayDownZone(List<PhomCard> cards, GameObject zone)
    {
        //PhomUtil.ResetCardsState(cards);
        //base.AnimateSortLayDownZone(cards, zone);
        if (cards == null)
            return;

        PhomUtil.ResetCardsState(cards);

        foreach (PhomCard card in cards)
        {
            card.transform.SetParent(zone.transform);
            card.transform.localPosition = Vector3.zero;
            card.transform.localScale = new Vector3(0.7f, 0.7f, 1);
        }

        PhomUtil.SortCardsOrderInZone(cards, zone);
        //float distance = 126f * Screen.width / 1600 * 0.7f;
        float distance = Screen.width * PhomConst.SMALL_HORIZONTAL_RATIO; // hen chi sai
        Vector3 center = zone.transform.position;
        float x = center.x - zone.transform.childCount / 2 * distance;

        if (zone.transform.childCount % 2 == 0)
            x += distance / 2;

        //Vector3 startPoint = zone.transform.position;
        //float x = startPoint.x;
        Hashtable hash = new Hashtable();
        hash.Add("time", 0.1f);
        hash.Add("position", null);
        hash.Add("easetype", iTween.EaseType.linear);

        foreach (Transform t in zone.transform)
        {
            hash["position"] = new Vector3(x, center.y);
            iTween.MoveTo(t.gameObject, hash);
            x += distance;
        }
    }

    public override void AnimateStartTurn(bool isStartGame, List<PhomCard> recommendedEatenCards)
    {
        if (isStartGame)
        {
            HideButton(PhomConst.DRAW_BUTTON_INDEX);
            ShowButton(PhomConst.DISCARD_BUTTON_INDEX);
        }
        else
        {
            ShowButton(PhomConst.DRAW_BUTTON_INDEX);
            HideButton(PhomConst.DISCARD_BUTTON_INDEX);
        }

        if (recommendedEatenCards == null || recommendedEatenCards.Count < 2)
        {
            HideButton(PhomConst.MELD_BUTTON_INDEX);
        }
        else
        {
            PhomRoomController.Instance.IsAutoPickingCard = true;
            foreach (PhomCard card in recommendedEatenCards)
                PickCard(card);
            PhomRoomController.Instance.IsAutoPickingCard = false;
            ShowButton(PhomConst.MELD_BUTTON_INDEX);
        }

        ButtonsZone.SetActive(true);
        CountDownTime();
    }

    public override void AnimateEndTurn()
    {
        ButtonsZone.SetActive(false);
        StopCountDownTime();
    }

    #endregion

    public void PickCard(PhomCard card)
    {
        SoundManager.PlaySound(SoundManager.SELECT_CARD);
        if (!Combination.All.Contains(card))
            return;

        lock (this)
        {
            Vector3 cardPos = card.transform.position;
            if (CurrentSelectedCards.Contains(card))
            {
                CurrentSelectedCards.Remove(card);
                card.transform.position = new Vector3(cardPos.x, cardPos.y - Screen.height * PhomConst.VERTICAL_RATIO);
            }
            else
            {
                CurrentSelectedCards.Add(card);
                card.transform.position = new Vector3(cardPos.x, cardPos.y + Screen.height * PhomConst.VERTICAL_RATIO);
            }

            if (PhomUtil.IsMeld(CurrentSelectedCards)
                && !PhomUtil.IsMeldInCombination(CurrentSelectedCards, Combination))
                ShowButton(PhomConst.CHANGE_COMBO_BUTTON_INDEX);
            else
                HideButton(PhomConst.CHANGE_COMBO_BUTTON_INDEX);
        }
    }

    public void PickRecommendedLaidDownCards()
    {
        PhomRoomController.Instance.IsAutoPickingCard = true;
        CurrentSelectedCards.Clear();
        foreach (PhomCard card in Combination.ChosenMeld)
            PickCard(card);
        foreach (List<PhomCard> cards in Combination.Runs)
            foreach (PhomCard card in cards)
                PickCard(card);
        foreach (List<PhomCard> cards in Combination.Sets)
            foreach (PhomCard card in cards)
                PickCard(card);
        PhomRoomController.Instance.IsAutoPickingCard = false;
        if (MeldBorders[0].activeSelf)
            MeldBorders[0].transform.position = new Vector3(
                MeldBorders[0].transform.position.x,
                MeldBorders[0].transform.position.y + Screen.height * PhomConst.VERTICAL_RATIO);

        if (MeldBorders[1].activeSelf)
            MeldBorders[1].transform.position = new Vector3(
                MeldBorders[1].transform.position.x,
                MeldBorders[1].transform.position.y + Screen.height * PhomConst.VERTICAL_RATIO);

        if (MeldBorders[2].activeSelf)
            MeldBorders[2].transform.position = new Vector3(
                MeldBorders[2].transform.position.x,
                MeldBorders[2].transform.position.y + Screen.height * PhomConst.VERTICAL_RATIO);
    }

    public void DecideToShowLayDownOrDiscardButton()
    {
        if (DiscardedCards.Count == 3 
            && (Combination.ChosenMeld.Count > 0 || Combination.Runs.Count > 0 || Combination.Sets.Count > 0))
        {
            PickRecommendedLaidDownCards();
            ShowButton(PhomConst.LAY_DOWN_BUTTON_INDEX);
        }
        else
        {
            ShowButton(PhomConst.DISCARD_BUTTON_INDEX);
        }
    }

    public void DecideToShowSendCardOrDiscardButton(List<PhomCard> recommendedSentCards)
    {
        if (recommendedSentCards.Count > 0)
        {
            PhomRoomController.Instance.IsAutoPickingCard = true;
            foreach (PhomCard card in recommendedSentCards)
                PickCard(card);
            PhomRoomController.Instance.IsAutoPickingCard = false;
            ShowButton(PhomConst.SEND_CARD_BUTTON_INDEX);
            HideButton(PhomConst.DISCARD_BUTTON_INDEX);
        }
        else
        {
            HideButton(PhomConst.SEND_CARD_BUTTON_INDEX);
            ShowButton(PhomConst.DISCARD_BUTTON_INDEX);
        }
    }

    public void HideButton(int index)
    {
        ButtonsZone.transform.GetChild(index).gameObject.SetActive(false);
    }

    public void ShowButton(int index)
    {
        ButtonsZone.transform.GetChild(index).gameObject.SetActive(true);
    }
}