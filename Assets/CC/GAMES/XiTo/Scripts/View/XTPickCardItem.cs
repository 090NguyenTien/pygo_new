﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class XTPickCardItem : BaseCardItem {

	[SerializeField]
	Button btnCard;

	void Awake()
	{
		btnCard.onClick.AddListener (PickCardOnClick);
	}

	public void Init(int _index)
	{
		indexCard = _index;
	}

	void PickCardOnClick()
	{
		GameObject.FindGameObjectWithTag ("GameController").SendMessage ("PickCard", indexCard);
	}
}
