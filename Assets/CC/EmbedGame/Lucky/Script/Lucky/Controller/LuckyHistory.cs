﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using Sfs2X.Entities.Data;

public class LuckyHistory : MonoBehaviour {

    public GameObject RowPrefab;
    public GameObject Container;
    public ArrayList contents;
    void Start()
    {
        contents = new ArrayList();
    }

    public void Open()
    {       
        gameObject.SetActive(true);
        GamePacket gp = new GamePacket(LuckyMiniCommandKey.GetSicboHistory);
        SFS.Instance.SendRoomRequest(gp);
    }

	public void onExtensionResponse(GamePacket gp)
	{
		switch (gp.cmd)
		{
			case LuckyMiniCommandKey.GetSicboHistory:
			    GetSicboHistory(gp);
			break;		
		}
	}

	private void GetSicboHistory(GamePacket gp){
		var logTx = gp.param.GetSFSArray (LuckyMiniParamKey.LogTx);
        Debug.LogError("logTx "+ logTx.Count);
		for (int i = 0; i < Container.transform.childCount; i++)
		{
			var gameObj = Container.transform.GetChild(i).gameObject;
			gameObj.SetActive(false);
		}
		if (logTx != null) {
            if (logTx.Count > Container.transform.childCount) {
                int diff = logTx.Count - Container.transform.childCount;
                GameObject rowObj = null;
                for (int i = 0; i < diff; i++) {
                    rowObj = Instantiate(RowPrefab);
                    rowObj.transform.parent = Container.transform;
                    rowObj.transform.localScale = new Vector3(1, 1, 1);
                    rowObj.transform.localPosition = Vector3.zero;
                }
            }
			for (int i = 0; i < logTx.Count; i++) {
                GameObject rowObj = Container.transform.GetChild(i).gameObject;
				rowObj.SetActive (true);
				var content = rowObj.GetComponent<LuckyHistoryContent> ();
				content.Show (i+1, (SFSObject) logTx.GetSFSObject(i));
			}
		}
	}

    public void Close()
    {
        gameObject.SetActive(false);
        for(var i = 0; i < Container.transform.childCount; i++)
        {
            var gameobj = Container.transform.GetChild(i).gameObject;
            gameobj.SetActive(false);
            contents.Add(gameobj);
        }
    }
}
