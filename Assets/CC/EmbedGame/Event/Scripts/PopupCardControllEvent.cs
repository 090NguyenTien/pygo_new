﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using BaseCallBack;

public class PopupCardControllEvent : MonoBehaviour
{

    [SerializeField]
    private InputField CardPrice;
    [SerializeField]
    private Dropdown CardType;
    [SerializeField]
    private InputField Pin;
    [SerializeField]
    private InputField Serial;
    [SerializeField]
    private Button BtnNap, btnClosePopupCard;
    [SerializeField]
    private Text Warning, PriceConfrim;
    private string Price;
    [SerializeField]
    GameObject PanelConfrim, panelProcessingPayment;
    
    onCallBackString callBack;
   
    //iron_hammer, silver_hammer, gold_hammer)

    public void Init(string price, onCallBackString _callBack = null)
    {
        callBack = _callBack;
        //string.Format("{0:n0}", int.Parse(price));
        CardPrice.text = price;
        Price = price;
        //Id_pig = Id_Pig;
        Pin.contentType = InputField.ContentType.IntegerNumber;
        Pin.characterLimit = 20;
        Serial.contentType = InputField.ContentType.IntegerNumber;
        Serial.characterLimit = 20;
        Warning.text = "";
        BtnNap.onClick.RemoveListener(BtnNapClick);
        BtnNap.onClick.AddListener(BtnNapClick);
        if (btnClosePopupCard != null)
        {
            btnClosePopupCard.onClick.RemoveListener(ClosePopupCard);
            btnClosePopupCard.onClick.AddListener(ClosePopupCard);
        }

        setDataCardCombobox();
        gameObject.SetActive(true);

    }


    public void BtnNapClick()
    {
        bool check = CheckInput();

        MuaItemBuyCard();
    }

    public void ClosePanelConfrim()
    {
        PriceConfrim.text = "";
        PanelConfrim.SetActive(false);
    }


    private bool CheckInput()
    {
        if (Pin.text.Length <= 6)
        {
			Warning.text = "Mã nạp thẻ không hợp lệ!";
            return false;
        }
        if (Serial.text.Length <= 6)
        {
			Warning.text = "Mã serial không hợp lệ!";
            return false;
        }
        return true;
    }



    public void ShowPopupCard()
    {
        setDataCardCombobox();
        gameObject.SetActive(true);
    }

    internal void Init(string pRICE_THE_CAO, int v, object buyXamTheCaoComplete)
    {
        throw new NotImplementedException();
    }

    public void ClosePopupCard()
    {
        Warning.gameObject.SetActive(false);
        gameObject.SetActive(false);
        Price = "";
        Pin.text = "";
        Serial.text = "";
        CardType.value = 0;
        Warning.text = "";
    }

    public void buyPackByMobieCard(string price)//call editor
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        CardPrice.text = price;

        ShowPopupCard();
    }




    private void setDataCardCombobox()
    {
        //Debug.LogError(name + "Mobifone::::::" + GameHelper.dicConfig["Mobifone"]);
        //Debug.LogError(name + "Viettel::::::" + GameHelper.dicConfig["Viettel"]);
        //Debug.LogError(name + "Vinaphone::::::" + GameHelper.dicConfig["Vinaphone"]);

        bool haveViettel = true;
        bool haveMobi = true;
        bool haveVina = true;
        if (int.Parse(GameHelper.dicConfig["Mobifone"]) == 0 || !GameHelper.CardPayment.cardMobi)
        {
            for (int i = 0; i < CardType.options.Count; i++)
            {
                if (CardType.options[i].text == "Mobifone")
                {
                    CardType.options.RemoveAt(i);
                    haveMobi = false;
                    break;
                }
            }
        }
        if (int.Parse(GameHelper.dicConfig["Viettel"]) == 0 || !GameHelper.CardPayment.cardVietel)
        {
            for (int i = 0; i < CardType.options.Count; i++)
            {
                if (CardType.options[i].text == "Viettel")
                {
                    CardType.options.RemoveAt(i);
                    haveViettel = false;
                    break;
                }
            }
        }
        if (int.Parse(GameHelper.dicConfig["Vinaphone"]) == 0 || !GameHelper.CardPayment.cardVina)
        {
            for (int i = 0; i < CardType.options.Count; i++)
            {
                if (CardType.options[i].text == "Vinaphone")
                {
                    CardType.options.RemoveAt(i);
                    haveVina = false;
                    break;
                }
            }
        }
        if (!haveMobi)
        {
            CardType.captionText.text = "Viettel";
            if (!haveViettel)
            {
                CardType.captionText.text = "Vinaphone";
                if (!haveVina)
                {
                    CardType.captionText.text = "";
                }
            }

        }
    }



    public void MuaItemBuyCard()
    {
        Debug.Log("mua Item Event======");
        string price = Price.ToString();
        string Type = CardType.captionText.text.ToLower();
        string pin = Pin.text;
        string serial = Serial.text;
        API.Instance.RequestMuaItemEvent(Type, price, pin, serial, MuaItemComplete);
        panelProcessingPayment.gameObject.SetActive(true);
    }
    private void MuaItemComplete(string s)
    {
        panelProcessingPayment.gameObject.SetActive(false);
        callBack(s);
        ClosePopupCard();
    }

    private void ClosePopupComplete()
    {
        // ClosePopupConfrim();
        ClosePopupCard();
    }


}
