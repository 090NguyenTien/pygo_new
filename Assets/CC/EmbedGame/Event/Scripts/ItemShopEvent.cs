﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemShopEvent : MonoBehaviour
{
    [SerializeField]
    GameObject TxtValue, BtnInapp, BtnCard, PriceInApp, PriceCard;
    [SerializeField]
    PopupCardControllEvent popupCard;
    //[SerializeField]
    string productId;
    int ChipEventVal;
    // Start is called before the first frame update
    void Start()
    {
        BtnCard.SetActive(false);
    }
    public void Init(string _productId, int value, Double priceInApp, int priceCard)
    {
        gameObject.SetActive(true);
        this.productId = _productId;
        ChipEventVal = value;
        TxtValue.GetComponent<Text>().text = value.ToString();
        BtnCard.SetActive(GameHelper.EnablePayment);
        this.PriceInApp.GetComponent<Text>().text = priceInApp.ToString();
        this.PriceCard.GetComponent<Text>().text = priceCard.ToString();

        BtnInapp.GetComponent<Button>().onClick.RemoveListener(BuyChipEventUseInapp);
        BtnInapp.GetComponent<Button>().onClick.AddListener(BuyChipEventUseInapp);
        BtnCard.GetComponent<Button>().onClick.RemoveListener(OnClickBuyWithCard);
        BtnCard.GetComponent<Button>().onClick.AddListener(OnClickBuyWithCard);
    }

    private void BuyChipEventUseInapp()
    {
        UnityPurchasinghelp.UnityPurchasingHelper.Instance.OnPurchaseClicked(productId, OnPuchased);
    }
    void OnPuchased(string productId, string purchaseToken, bool isSuccess)
    {

        if (isSuccess)
        {           
#if UNITY_ANDROID            
            string store = "googleplay";
            string productID = productId;//node["productId"].Value;
            string token = purchaseToken;//node["purchaseToken"].Value;
            API.Instance.RequestPaymentEventInAppAndroid(store, productID, token, RspPaymentInApp);
#endif
        }
        else
        {
            AlertController.api.showAlert("Giao dịch không thành công.");
        }
    }
    void RspPaymentInApp(string _json)
    {
        EventController.isBuyShop = true;
        
        JSONNode node = JSONNode.Parse(_json);
        AlertController.api.showAlert(node["msg"].Value);
        //Debug.Log(" RspPaymentEventTaiXiuInApp " + _json);
        //if (node["status"].AsInt == 1)
        //{            
        //    AlertController.api.showAlert(node["msg"].Value);
        //}
        //else if (node["status"].AsInt == 0)
        //{
        //    AlertController.api.showAlert(node["sms"]);
        //}

    }


    private void OnClickBuyWithCard()
    {
        popupCard.Init(this.PriceCard.GetComponent<Text>().text, OnBuyChipEventComplete);
    }

    private void OnBuyChipEventComplete(string _json)
    {
        EventController.isBuyShop = true;
        JSONNode node = JSONNode.Parse(_json);
        AlertController.api.showAlert(node["msg"].Value);
        //Debug.Log("OnBuyChipEventComplete " + _json);
        //if (node["status"].AsInt == 1)
        //{            
        //    AlertController.api.showAlert(node["msg"].Value);
        //}
        //else if (node["status"].AsInt == 0)
        //{
        //    AlertController.api.showAlert(node["sms"]);
        //}
    }
}
