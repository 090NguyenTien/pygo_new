﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemLichSuVay : MonoBehaviour
{
    [SerializeField]
    Text txtStatus, txtChip, txtDate;
    // Start is called before the first frame update
    void Start()
    {
        
    }
	public void INIT(int type, long chip, string datetime){
        if (type == 1)
        {
            txtStatus.text = "VAY";
        }
        else if(type == 2)
        {
            txtStatus.text = "TRẢ NỢ GỐC";
        }
        else if (type == 3)
        {
            txtStatus.text = "TRẢ LÃI";
        }
        txtChip.text = Utilities.GetStringMoneyByLongBigSmall(chip);
        txtDate.text = datetime;
	}
}
