﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemKethu : MonoBehaviour
{
    // Start is called before the first frame update
    public Text TenKethu;
    string IdKethu;
    void Start()
    {
        
    }
    public void Init(string _idKethu, string _nameKethu)
    {
        IdKethu = _idKethu;
        TenKethu.text = _nameKethu;
    }
    public void TanCong()
    {
        FortuneWheelManager.api.SetDataClickKeThu(IdKethu, TenKethu.text);
        Close();
    }
    public void Close()
    {
        Destroy(gameObject);
    }
}
