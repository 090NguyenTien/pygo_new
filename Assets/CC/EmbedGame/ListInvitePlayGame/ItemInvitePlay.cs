﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemInvitePlay : MonoBehaviour
{
    [SerializeField]
    Text inviteContent;
    string username, pass;
    int roomId, gameId, roomLevel;
    long bet, betRequire; 
    // Start is called before the first frame update
    void Start()
    {

    }
    public void Init(Hashtable itemInvite)
    {

        username = itemInvite["_username"].ToString();
        roomId = (int)itemInvite["_roomID"];
        gameId = (int)itemInvite["_gameID"];
        roomLevel = (int)itemInvite["_roomLevel"];
        bet = (long)itemInvite["_bet"];
        betRequire = (long)itemInvite["_betRequire"];
        pass = itemInvite["_pass"].ToString();
        string gameName = GameHelper.GetNameGame((GAMEID)gameId);
        inviteContent.text = username + " Mời bạn chơi game : " + gameName + " - Phòng : " + roomId + " - Mức cược : " + bet;
    }
    public void DongY()
    {

    }
    public void Cancel()
    {

    }
}