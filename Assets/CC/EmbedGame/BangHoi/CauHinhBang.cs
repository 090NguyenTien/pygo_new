﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CauHinhBang : MonoBehaviour
{
    [SerializeField]
    Dropdown DropDownVip;
    [SerializeField]
    Toggle ToggleDuyet;
    int valueDropdown;

    [SerializeField]
    Toggle AllowCuuTro;
    [SerializeField]
    InputField MaxChipCuuTro;
    // Start is called before the first frame update
    void Start()
    {
        DropDownVip.onValueChanged.AddListener(delegate {
            myDropdownValueChangedHandler(DropDownVip);
        });
        //ToggleDuyet.onValueChanged.AddListener(isToggleChange);
    }
    //public void isToggleChange(bool val)
    //{
    //    if (val == true)
    //    {
    //        if (ToggleDuyet.isOn)
    //        {

    //        }
    //    }
    //}
    void Destroy()
    {
        DropDownVip.onValueChanged.RemoveAllListeners();
    }

    private void myDropdownValueChangedHandler(Dropdown target)
    {
        valueDropdown = target.value;
    }
    public void SetDropdownIndex(int index)
    {
        DropDownVip.value = index;
    }
    public void Show()
    {
        if (InfoBangHoiController.is_public == 0)
        {
            ToggleDuyet.isOn = true;
        }
        else
        {
            ToggleDuyet.isOn = false;
        }
        SetDropdownIndex(InfoBangHoiController.min_vip);

        if (InfoBangHoiController.is_deputyhelp == 0) AllowCuuTro.isOn = false;
        else AllowCuuTro.isOn = true;

        MaxChipCuuTro.text = InfoBangHoiController.max_deputyhelp.ToString();
    }
    public void DongY()
    {
        if (long.Parse(MaxChipCuuTro.text) < 1000000 && AllowCuuTro.isOn==true)
        {
            AlertController.api.showAlert("Chip Cứu Trợ Tối Thiểu Là 1.000.000 Chip");
            return;
        }else if (long.Parse(MaxChipCuuTro.text) > InfoBangHoiController.chip && AllowCuuTro.isOn == true)
        {
            AlertController.api.showAlert("Chip Cứu Trợ Không Được Nhiều Hơn Bang Khố");
            return;
        }
        GamePacket gp = new GamePacket(CommandKey.SET_UP_CLAN);
        gp.Put("is_public", ToggleDuyet.isOn?0:1);
        gp.Put("min_vip", DropDownVip.value);
        gp.Put("is_deputyhelp", AllowCuuTro.isOn ? 1 : 0);
        gp.Put("max_deputyhelp", long.Parse(MaxChipCuuTro.text));
        gp.Put("clan_rule", InfoBangHoiController.clan_rule);
        gp.Put("clan_notice", InfoBangHoiController.clan_notice);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void OnChatResponse(GamePacket param)
    {
        switch (param.cmd)
        {
            //case CommandKey.REQUEST_LEAVE_CLAN:
                //LeaveClanRes(param);
                //break;
        }
    }

    
}
