﻿using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DanhSachBangHoi : MonoBehaviour
{
    [SerializeField]
    GameObject container;
    [SerializeField]
    ItemBangHoiController itemBanghoi;
    List<ItemBangHoiController> lstItemBangHoi;
    [SerializeField]
    Toggle TogglePhuBang, ToggleDaiBang;
    int curToggle = 1;
    // Start is called before the first frame update
    void Start()
    {
        for (int z = 0; z < container.transform.childCount; z++)
        {
            Destroy(container.transform.GetChild(z).gameObject);
        }
        lstItemBangHoi = new List<ItemBangHoiController>();
        GamePacket gp = new GamePacket(CommandKey.GET_CLAN_LIST);
        gp.Put("top_type", 1);
        SFS.Instance.SendZoneRequest(gp);

        TogglePhuBang.onValueChanged.AddListener(isToggleChange);
        ToggleDaiBang.onValueChanged.AddListener(isToggleChange);
    }
    void ClearListBang()
    {
        for (int z = 0; z < container.transform.childCount; z++)
        {
            Destroy(container.transform.GetChild(z).gameObject);
        }
    }
    public void isToggleChange(bool val)
    {
        if (val == true)
        {
            if (TogglePhuBang.isOn && curToggle!=1)
            {
                ClearListBang();
                curToggle = 1;
                GamePacket gp = new GamePacket(CommandKey.GET_CLAN_LIST);
                gp.Put("top_type", 1);
                SFS.Instance.SendZoneRequest(gp);
            }else if (ToggleDaiBang.isOn && curToggle!=2)
            {
                ClearListBang();
                curToggle = 2;
                GamePacket gp = new GamePacket(CommandKey.GET_CLAN_LIST);
                gp.Put("top_type", 2);
                SFS.Instance.SendZoneRequest(gp);
            }
        }
    }

    public void OnChatResponse(GamePacket param)
    {
        switch (param.cmd)
        {
            case CommandKey.GET_CLAN_LIST:
                GetClanList(param);
                break;
            case CommandKey.REQUEST_JOIN_CLAN:
                JoinClanRsp(param);
                break;
        }
    }
    private void JoinClanRsp(GamePacket param)
    {
        int status = param.GetInt("status");
        
        AlertController.api.showAlert(param.GetString("msg"));

        if (status == 1)
        {
            int member_status = param.GetInt("member_status");
            if (member_status == 1)
            {
                foreach (ItemBangHoiController item in lstItemBangHoi)
                {
                    item.VisibleButtonJoin(false);
                }
            }            
        }
        
    }
    private void GetClanList(GamePacket param)
    {
        Debug.Log(param);
        ISFSArray clans = param.GetSFSArray("clans");

        foreach(SFSObject clan in clans)
        {
            ItemBangHoiController item = Instantiate(itemBanghoi, container.transform) as ItemBangHoiController;
            item.InitInfo(clan);
            lstItemBangHoi.Add(item);
        }
    }
   
    public void ClosePanel()
    {        
        Destroy(gameObject);
    }
}
