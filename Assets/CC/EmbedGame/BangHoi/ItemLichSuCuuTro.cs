﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemLichSuCuuTro : MonoBehaviour
{
    [SerializeField]
    Text TxtChucVu, TxtUserCuuTro, TxtChip, TxtDate;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void InitItem(string chucvu, string usercuutro, long chip, string date)
    {
        TxtChucVu.text = chucvu;
        TxtUserCuuTro.text = usercuutro;
        TxtChip.text = Utilities.GetStringMoneyByLong(chip);
        TxtDate.text = date.ToString();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
