﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CuuTroController : MonoBehaviour
{
    
    [SerializeField]
    GameObject ViewQuyenGop;
    [SerializeField]
    Text TxtChipQuyenGop;
    long chipCuuTro;
    long ChipToiThieuGiuLai=10000000;
    long ChipToiThieuQuyenGop = 1000000;
    // Start is called before the first frame update
    void Start()
    {
        ViewQuyenGop.SetActive(false);
        chipCuuTro = 0;
        TxtChipQuyenGop.text = "0";
    }
    public void ShowViewQuyenGop()
    {
        ViewQuyenGop.SetActive(true);
        chipCuuTro = 0;
        TxtChipQuyenGop.text = "0";
    }
    public void HideViewQuyenGop()
    {
        ViewQuyenGop.SetActive(false);
    }
    public void XinCuuTro()
    {
        GamePacket gp = new GamePacket(CommandKey.REQUEST_CUU_TRO);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }

    public void AddChipQuyenGop(int val=0)
    {
        chipCuuTro += val;
        TxtChipQuyenGop.text = Utilities.GetStringMoneyByLongBigSmall(chipCuuTro);
    }
    public void QuyenGop()
    {
        if(chipCuuTro < ChipToiThieuQuyenGop)
        {
            AlertController.api.showAlert("Số Tiền Quyên Góp Tối Thiểu Là 1M");
            return;
        }else if (MyInfo.CHIP - chipCuuTro < ChipToiThieuGiuLai) 
        {
            AlertController.api.showAlert("Số Tiền Còn Lại Trong Tài Khoản Tối Thiểu Phải Là 10M");
            return;
        }
        GamePacket gp = new GamePacket(CommandKey.CONTRIBUTE_CLAN);
        gp.Put("chip", chipCuuTro);
        SFS.Instance.SendRoomBangHoiRequest(gp);
        chipCuuTro = 0;
        TxtChipQuyenGop.text = "0";
    }
}
