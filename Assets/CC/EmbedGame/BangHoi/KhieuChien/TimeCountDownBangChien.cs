﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class TimeCountDownBangChien : MonoBehaviour
{
    [SerializeField]
    private Text txtTime,txtDes;

    [SerializeField]
    private bool isCompleteDisable = true;

    [SerializeField]
    private string textComplete = "";

    private int timeCountDown; // tinh don vi la giay
    //private int time
    private bool isInited;
    private int timeInit;//thoi diem init tinh bang giay
    private Action callBack;
    private int baseCountDownTime = 0;
    public bool isRunning()
    {
        return isInited;
    }
    public void CloseTimeCountDown()
    {
        gameObject.SetActive(false);
    }
    /// <summary>
    /// count down with time in second
    /// </summary>
    /// <param name="baseTimeCountDown">base time (ex: 3600)</param>
    /// <param name="timeSecondCountDown">time need countdown (ex: 3000)</param>
    /// <param name="timeInit">time begin init</param>
    /// <param name="callBack">call back done</param>
    public void initTimeCountDown(int baseTimeCountDown,int timeSecondCountDown, int timeInit, Action callBack)
    {
        this.callBack = null;
        this.callBack = callBack;
        this.timeInit = timeInit + (int)Time.realtimeSinceStartup;
        this.timeCountDown =  timeSecondCountDown;
        this.baseCountDownTime = baseTimeCountDown;
        gameObject.SetActive(true);
        isInited = true;
        Debug.Log("baseTimeCountDown "+ baseTimeCountDown);
        if (gameObject.activeInHierarchy == true)
        {
            StartCoroutine(UpdateTime());
        }
        
    }

    void OnEnable()
    {
        if (isInited)
        {
            StartCoroutine(UpdateTime());
        }
    }
    
    public void PauseCountDown()
    {
        isInited = false;
    }
    IEnumerator UpdateTime()
    {        
        while (true)
        {
            int curTime = (int)Time.realtimeSinceStartup;
            //Debug.LogError("==UpdateTime==============curTime"+ curTime);
            int currentCountdown = timeCountDown - (curTime - timeInit);
            //Debug.LogError("==UpdateTime==============currentCountdown" + currentCountdown);
            if (currentCountdown > 0)
            {
                if(isInited) txtTime.text = Ulti.convertCountDownTimeToText(currentCountdown);
            }
            //if (!isInited)
            //{
            //    StopCoroutine("UpdateTime");
            //    yield break;
            //}
            else
            {
                isInited = false;
                if (isCompleteDisable == false)
                {
                    txtTime.text = textComplete;
                }
                Debug.Log("==UpdateTime==============isCompleteDisable" + isCompleteDisable);
                gameObject.SetActive(!isCompleteDisable);
                if (callBack != null)
                {
                    callBack();
                }
                yield break;
            }
            yield return new WaitForSeconds(1);
        }
    }
    public int getTimeLeft()
    {
        return timeCountDown - ((int)Time.realtimeSinceStartup - timeInit);

    }
    public void SetDescription(string str)
    {
        txtDes.text = str;
    }
}
