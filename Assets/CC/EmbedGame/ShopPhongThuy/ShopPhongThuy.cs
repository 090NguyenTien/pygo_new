﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopPhongThuy : MonoBehaviour
{
    [SerializeField]
    ItemPT itempt;
    [SerializeField]
    ItemHisPT itemHisPt;
    [SerializeField]
    GameObject Container, ContainerHistory, PopupBuy, PopupDetail, GroupBuy, PopupHistory;
    //popup buy
    [SerializeField]
    UnityEngine.UI.InputField inputSoluong, inputChipCan, inputTen, inputSDT, inputSonha, inputTenDuong, inputPhuongXa, inputQuan, inputThanhPho;
    [SerializeField]
    Button btnBuy, btnClosePoupBuy, btnCloseShop, btnOpenHistory, btnCloseHistory;
    
    //popup detail
    [SerializeField]
    Text txtDes;
    [SerializeField]
    Button btnCloseDes;
    //end
    string idItem="", priceItem="0";
    long chipcan;
    // Start is called before the first frame update
    void Start()
    {
        PopupBuy.SetActive(false);
        PopupDetail.SetActive(false);
        GroupBuy.SetActive(true);
        btnCloseDes.onClick.AddListener(onClosePopDetail);
        btnClosePoupBuy.onClick.AddListener(onClosePopBuy);
        btnBuy.onClick.AddListener(onBuyItem);
        btnOpenHistory.onClick.AddListener(onOpenHistory);
        btnCloseHistory.onClick.AddListener(onCloseHistory);
        btnCloseShop.onClick.AddListener(onCloseShopPT);
        inputSoluong.onValueChange.AddListener(onChangeInputQuantity);
        WWWForm form = new WWWForm();
        form.AddField("", "");
        API.Instance.BaseCallService("/event/shopitems", form, GetInfoShopComplete);
    }

    private void onCloseHistory()
    {
        GroupBuy.SetActive(true);
        PopupHistory.SetActive(false);
    }

    private void onOpenHistory()
    {
        GroupBuy.SetActive(false);
        PopupHistory.SetActive(true);
        WWWForm form = new WWWForm();
        form.AddField("", "");
        API.Instance.BaseCallService("/event/orderhistory", form, GetHistoryShopComplete);
    }

    private void GetHistoryShopComplete(string s)
    {
        Debug.Log("/event/orderhistory====" + s);
        JSONNode data = JSONNode.Parse(s);
        for (int z = 0; z < ContainerHistory.transform.childCount; z++)
        {
            Destroy(ContainerHistory.transform.GetChild(z).gameObject);
        }

       
        for (int i = 0; i < data.Count; i++)
        {
            ItemHisPT comp = Instantiate(itemHisPt, ContainerHistory.transform) as ItemHisPT;
            comp.init(data[i]);
        }
    }

    private void onCloseShopPT()
    {
        Destroy(gameObject);
    }

    private void onChangeInputQuantity(string arg0)
    {
        int soluong = int.Parse(inputSoluong.text);
        chipcan = soluong * long.Parse(priceItem);
        inputChipCan.text = Utilities.GetStringMoneyByLongBigSmall(chipcan);
    }

    private void onClosePopBuy()
    {
        PopupBuy.SetActive(false);
    }

    private void onBuyItem()
    {
        if (int.Parse(inputSoluong.text) < 1)
        {
            AlertController.api.showAlert("Số Lượng Mua Ít Nhất Là 1");
            return;
        }        
        if (chipcan > MyInfo.CHIP)
        {
            AlertController.api.showAlert("Bạn Không Đủ Chip Để Mua");
            return;
        }
        if (inputTen.text=="")
        {
            AlertController.api.showAlert("Bạn Chưa Nhập Tên");
            return;
        }
        if (inputSDT.text == "")
        {
            AlertController.api.showAlert("Bạn Chưa Nhập Số Điện Thoại");
            return;
        }
        if (inputSonha.text == "")
        {
            AlertController.api.showAlert("Bạn Chưa Nhập Số Nhà");
            return;
        }
        if (inputTenDuong.text == "")
        {
            AlertController.api.showAlert("Bạn Chưa Nhập Tên Đường");
            return;
        }
        if (inputPhuongXa.text == "")
        {
            AlertController.api.showAlert("Bạn Chưa Nhập Phường Xã");
            return;
        }
        if (inputQuan.text == "")
        {
            AlertController.api.showAlert("Bạn Chưa Nhập Quận");
            return;
        }
        if (inputThanhPho.text == "")
        {
            AlertController.api.showAlert("Bạn Chưa Nhập Thành Phố");
            return;
        }
        string address = "Tên:" + inputTen.text + ", Điện thoại:" + inputSDT.text + ", Số nhà:" + inputSonha.text + ", Đường:" + inputTenDuong.text + ", Phường:" + inputPhuongXa.text + ", Quận:" +
            inputQuan.text + ", Thành Phố:" + inputThanhPho.text;
        WWWForm form = new WWWForm();
        form.AddField("item", idItem);
        form.AddField("qty", inputSoluong.text);
        form.AddField("address", address);
        API.Instance.BaseCallService("/event/buyshopitem", form, BuyShopPhongThuyComplete);
    }

    private void BuyShopPhongThuyComplete(string s)
    {
        Debug.Log("/event/buyshopitem====" + s);
        JSONNode data = JSONNode.Parse(s);
        if (int.Parse(data["status"]) == 1)
        {
            AlertController.api.showAlert("Đặt Hàng Thành Công, Đơn Hàng Sẽ Được Giao Từ 5 - 7 Ngày");
            MyInfo.CHIP = long.Parse(data["chip"]);
            GamePacket gp = new GamePacket(CommandKey.REFRESH);
            SFS.Instance.SendZoneRequest(gp);
        }
        else
        {
            AlertController.api.showAlert(data["msg"].ToString());
        }
    }

    private void onClosePopDetail()
    {
        PopupDetail.SetActive(false);
    }

    private void GetInfoShopComplete(string s)
    {
        Debug.Log("/event/shopitems====" + s);
        for (int z = 0; z < Container.transform.childCount; z++)
        {
            Destroy(Container.transform.GetChild(z).gameObject);
        }

        JSONNode data = JSONNode.Parse(s);
        if (int.Parse(data["status"]) == 1)
        {
            data = data["data"];
            for (int i = 0; i < data.Count; i++)
            {
                ItemPT comp = Instantiate(itempt, Container.transform) as ItemPT;
                comp.init(data[i], onItemClick);
            }
        }
        else
        {
            AlertController.api.showAlert("Đã Hết Giờ Diễn Ra Sự Kiện!");
            onCloseShopPT();
        }        
    }
    
    private void onItemClick(string s, int type)
    {
        if (type == 0)//show detail
        {
            PopupDetail.SetActive(true);
            txtDes.text = s;
            PopupBuy.SetActive(false);
        }
        else if(type == 1)//show poup Buy
        {
            PopupDetail.SetActive(false);
            PopupBuy.SetActive(true);
            inputSoluong.text = "1";
            string[] aStr = s.Split(',');
            idItem = aStr[0];
            priceItem = aStr[1];
            int soluong = int.Parse(inputSoluong.text);
            chipcan = soluong * long.Parse(priceItem);
            inputChipCan.text = Utilities.GetStringMoneyByLongBigSmall(chipcan);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
