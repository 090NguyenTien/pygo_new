﻿using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatisticalLotteryManager : MonoBehaviour {

    private static StatisticalLotteryManager instance;
    public static StatisticalLotteryManager Instance { get { return instance; } }
    [SerializeField]
    GameObject Panel;
    [SerializeField]
    GameObject List_AppearNumber;
    [SerializeField]
    GameObject Module_AppearNumber;
    [SerializeField]
    Transform Parent_AppearNumber;

    [SerializeField]
    PopupLotteryManager LotteryManager;

    [SerializeField]
    GameObject List_NotAppearNumber;
    [SerializeField]
    GameObject Module_NotAppearNumber;
    [SerializeField]
    Transform Parent_NotAppearNumber;


    [SerializeField]
    GameObject List_HotNumber;
    [SerializeField]
    Transform Parent_HotNumber;

    [SerializeField]
    GameObject Panel_Appear;
    [SerializeField]
    GameObject Panel_NotAppear;
    [SerializeField]
    GameObject Panel_HotNumber;
    [SerializeField]
    Button BtnAppear;
    [SerializeField]
    Button BtnNotAppear;
    [SerializeField]
    Button BtnHotNumber;

    private string[] ArrayAppearRepones;
    private string[] ArrayNotAppearRepones;
    private string[] ArrayHotNumberRepones;

    private SFSObject obj;
    ISFSArray myArray;
    private int[] DiceResult;
    private string GameResult;
    void Awake()
    {
        instance = this;

    }

    #region REQUEST
    public void SendRequest()
    {
        SendSoXuatHienRequest();
        SendSoKhongXuatHienRequest();
        SendSoDatCuocRequest();
    }

    private void SendSoXuatHienRequest()
    {
        GamePacket gp = new GamePacket("AppearNumber");
        SFS.Instance.SendRoomRequest(gp);
    }


    private void SendSoKhongXuatHienRequest()
    {
        GamePacket gp = new GamePacket("NotAppearNumber");
        SFS.Instance.SendRoomRequest(gp);
    }

    private void SendSoDatCuocRequest()
    {
        GamePacket gp = new GamePacket("HotNumber");
        SFS.Instance.SendRoomRequest(gp);
    }

    #endregion

    #region REPONES

    public void SetArrayAppearRepones(GamePacket gp)
    {
        ArrayAppearRepones = gp.GetStringArray("numbers");

        for (int i = 0; i < Parent_AppearNumber.childCount; i++)
        {
            GameObject.Destroy(Parent_AppearNumber.GetChild(i).gameObject);
        }
        ShowNumbers(Parent_AppearNumber, ArrayAppearRepones);
    }

    public void SetArrayNotAppearRepones(GamePacket gp)
    {
        ArrayNotAppearRepones = gp.GetStringArray("numbers");
    }

    public void SetArrayHotNumberRepones(GamePacket gp)
    {
        ArrayHotNumberRepones = gp.GetStringArray("numbers");
    }

    #endregion


    #region 10 SỐ XUẤT HIÊN

    public void AppearNumber()
    {
        PopupLotteryManager.Panel = "ThongKe";
        LotteryManager.BtnBet.gameObject.SetActive(true);
        List_AppearNumber.SetActive(false);
        Panel.SetActive(true);
        Panel_Appear.SetActive(true);
        Panel_NotAppear.SetActive(false);
        Panel_HotNumber.SetActive(false);

        BtnAppear.onClick.RemoveAllListeners();
        BtnAppear.onClick.AddListener(ButtonXuatOnClick);

        BtnNotAppear.onClick.RemoveAllListeners();
        BtnNotAppear.onClick.AddListener(ButtonAnOnClick);

        BtnHotNumber.onClick.RemoveAllListeners();
        BtnHotNumber.onClick.AddListener(ButtonDatOnClick);

        ArrayAppearRepones = new string[10];
        ArrayNotAppearRepones = new string[10];
        ArrayHotNumberRepones = new string[10];

        List_AppearNumber.SetActive(true);
        SendRequest();               
    }

    public void BackUpInit()
    {
        List_AppearNumber.SetActive(false);
        Panel.SetActive(false);
        Panel_Appear.SetActive(true);
        Panel_NotAppear.SetActive(false);
        Panel_HotNumber.SetActive(false);
              
        ShowNumbers(Parent_AppearNumber, ArrayAppearRepones);
    }
       
    #endregion


    #region 10 SỐ KHÔNG XUẤT HIỆN

    public void NotAppearNumber()
    {
        List_NotAppearNumber.SetActive(true);

        for (int i = 0; i < Parent_NotAppearNumber.childCount; i++)
        {
            GameObject.Destroy(Parent_NotAppearNumber.GetChild(i).gameObject);
        }
        ShowNumbers(Parent_NotAppearNumber, ArrayNotAppearRepones);
    }

    #endregion


    #region 10 SỐ ĐẶT CƯỢC NHIỀU NHẤT

    public void HotNumber()
    {
        List_HotNumber.SetActive(true);

        for (int i = 0; i < Parent_HotNumber.childCount; i++)
        {
            GameObject.Destroy(Parent_HotNumber.GetChild(i).gameObject);
        }

        ShowNumbers(Parent_HotNumber, ArrayHotNumberRepones);
    }
   
    #endregion


    #region BUTTON



    public void ButtonXuatOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        Panel_Appear.SetActive(true);
        Panel_NotAppear.SetActive(false);
        Panel_HotNumber.SetActive(false);
    }

    public void ButtonAnOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        Panel_Appear.SetActive(false);
        Panel_NotAppear.SetActive(true);
        Panel_HotNumber.SetActive(false);

        NotAppearNumber();
    }

    public void ButtonDatOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        Panel_Appear.SetActive(false);
        Panel_NotAppear.SetActive(false);
        Panel_HotNumber.SetActive(true);

        HotNumber();
    }

    #endregion

    private void ShowNumbers(Transform Parent, string[] ArrayNumberType)
    {
        int dem = 0;
        string SoTruoc = "";

        for (int i = 0; i < ArrayNumberType.Length; i++)
        {
            if (dem == 0)
            {
                SoTruoc = ArrayNumberType[i];
                dem = 1;
            }
            else
            {
                ShowItemNumber(Parent, i - 1, SoTruoc, ArrayNumberType[i]);
                dem = 0;
                SoTruoc = "";
            }
        }
    }



    void ShowItemNumber(Transform Parent, int index, string number_1, string number_2)
    {
        SoKhongXuatHienModule itemView;
        GameObject obj = Instantiate(Module_NotAppearNumber) as GameObject;
        ItemNotAppearNumber item = new ItemNotAppearNumber();

        obj.transform.SetParent(Parent);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<SoKhongXuatHienModule>();

        itemView.Init(item);
        itemView.Show(number_1, number_2);
    }

    public void Hide()
    {
        Panel.SetActive(false);
    }

    public void UpdateHistory(int[] diceResult, string gameResult)
    {
        DiceResult = diceResult;
        GameResult = gameResult;

        if (Panel.activeSelf)
            AppearNumber();
    }
}
